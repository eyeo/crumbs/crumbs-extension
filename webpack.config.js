const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const { EnvironmentPlugin } = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const { sentryWebpackPlugin } = require('@sentry/webpack-plugin');

const manifest = require('./manifest.json');

const BUILD_DIR = path.resolve(__dirname, 'dist');
const SRC_DIR = path.resolve(__dirname, 'src');
const PACKAGES_DIR = path.resolve(__dirname, 'packages');
const APP_DIR = path.resolve(__dirname, 'app');
const ASSETS_DIR = path.resolve(__dirname, 'app/assets');
const DEPENDENCIES_DIR = path.resolve(__dirname, 'node_modules');
const POPUP_DIR = path.resolve(__dirname, 'app/popup');
const OPTIONS_DIR = path.resolve(__dirname, 'app/options');
const EMAIL_RELAY_DIR = path.resolve(__dirname, 'app/email-relay-menu');

const EASYPRIVACY_RULESET_ID = 'D72B6F06-52B2-4FED-96A2-1BF59CDD7AEC';
const CRUMBS_PRIVACY_FRIENDLY_RULESET_ID = '0798B6A2-94A4-4ADF-89ED-BEC112FC4C7F';
const COOKIELIST_RULESET_ID = '2090F374-29D9-4202-B2CE-139D6492D95E';
const DEMO_INTERESTS_RULESET_ID = 'demo-interests';

module.exports = (env, argv) => ({
  devtool: 'source-map',
  performance: {
    hints: false,
  },
  optimization: {
    usedExports: true,
    minimize: true,
  },
  entry: {
    background: [`${SRC_DIR}/background.js`],
    popup: [`${POPUP_DIR}/index.jsx`],
    options: [`${OPTIONS_DIR}/index.jsx`],
    'email-relay-menu': [`${EMAIL_RELAY_DIR}/index.jsx`],
    'email-relay': [`${SRC_DIR}/contentScripts/email-relay.js`],
    'relay-auth': [`${SRC_DIR}/contentScripts/relay-auth.js`],
    'first-time-use': [`${SRC_DIR}/contentScripts/first-time-use.js`],
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  output: {
    filename: '[name].js',
    path: BUILD_DIR,
    clean: true,
  },
  plugins: [
    new ESLintPlugin(),
    new EnvironmentPlugin({
      ENV_NAME: env.beta ? 'beta' : argv.mode,
      ADDON_NAME: manifest.short_name,
      ADDON_VERSION: `${manifest.version}${argv.mode === 'development' ? '-dev' : ''}`,
      EASYPRIVACY_RULESET_ID,
      CRUMBS_PRIVACY_FRIENDLY_RULESET_ID,
      COOKIELIST_RULESET_ID,
      DEMO_INTERESTS_RULESET_ID,
      SENTRY_DSN: process.env.SENTRY_DSN || '',
      TELEMETRY_AUTHORIZATION_TOKEN: process.env.TELEMETRY_AUTHORIZATION_TOKEN || '',
    }),
    new CopyPlugin({
      patterns: [
        {
          from: 'manifest.json',
          to: 'manifest.json',
          transform(content) {
            let s = content.toString();
            s = s.replace(/__EASYPRIVACY_RULESET_ID__/g, EASYPRIVACY_RULESET_ID);
            s = s.replace(/__CRUMBS_PRIVACY_FRIENDLY_RULESET_ID__/g, CRUMBS_PRIVACY_FRIENDLY_RULESET_ID);
            s = s.replace(/__COOKIELIST_RULESET_ID__/g, COOKIELIST_RULESET_ID);
            s = s.replace(/__DEMO_INTERESTS_RULESET_ID__/g, DEMO_INTERESTS_RULESET_ID);

            if (env.beta) {
              s = s.replace(/__MSG_name_releasebuild__/g, 'Crumbs (beta)');
            }

            return s;
          },
        },
        { // add generated DNR rulesets
          from: `${DEPENDENCIES_DIR}/@eyeo/webext-ad-filtering-solution/scriptsOutput/rulesets`,
          to: 'rulesets',
        },
        { // add Crumbs DNR rulesets
          from: 'rulesets',
          to: 'rulesets',
        },
        {
          from: `${DEPENDENCIES_DIR}/@eyeo/webext-ad-filtering-solution/scriptsOutput/subscriptions`,
          to: 'subscriptions',
        },
        {
          from: `${DEPENDENCIES_DIR}/@eyeo/crumbs-ext-sdk/dist/content-scripts`,
          to: 'content-scripts',
        },
        {
          from: `${ASSETS_DIR}/icons/*.png`,
          to: 'icons/[name][ext]',
        },
        {
          from: `${ASSETS_DIR}/fonts/*.otf`,
          to: 'fonts/[name][ext]',
        },
        {
          from: `${ASSETS_DIR}/_locales`,
          to: '_locales',
        },
        { from: `${DEPENDENCIES_DIR}/webextension-polyfill/dist/browser-polyfill.js` },
        {
          from: `${DEPENDENCIES_DIR}/@eyeo/webext-ad-filtering-solution/dist/ewe-content.js`,
          to: 'ewe-content-script.js',
        },
        {
          from: `${POPUP_DIR}/index.html`,
          to: 'popup.html',
        },
        {
          from: `${OPTIONS_DIR}/index.html`,
          to: 'options.html',
        },

        {
          from: `${EMAIL_RELAY_DIR}/index.html`,
          to: 'crumbs-email-relay-menu.html',
        },
      ],
    }),
    // SENTRY_AUTH_TOKEN is exported in the CI release job
    ...(process.env.SENTRY_AUTH_TOKEN && argv.mode === 'production' ? [
      sentryWebpackPlugin({
        org: 'eyeo',
        project: 'crumbs-extension',
        release: {
          name: manifest.version,
        },
        // Auth tokens can be obtained from https://sentry.io/settings/account/api/auth-tokens/
        // and need `project:releases` and `org:read` scopes
        authToken: process.env.SENTRY_AUTH_TOKEN,
      }),
    ] : []),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [SRC_DIR, PACKAGES_DIR],
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              envName: 'src',
            },
          },
        ],
      },
      {
        test: /\.jsx$/,
        include: [APP_DIR],
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              envName: 'app',
            },
          },
        ],
      },
      {
        test: /\.css$/,
        include: [APP_DIR],
        use: [
          'style-loader', // inject CSS to page
          'css-loader', // translates CSS into CommonJS modules
          'postcss-loader', // Run postcss actions
        ],
      },
      {
        test: /\.(png|jp(e*)g|gif)$/,
        type: 'asset/resource',
      },
      {
        test: /\.(otf)$/,
        type: 'asset/resource',
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  resolve: {
    alias: {
      react: 'preact/compat',
      'react-dom/test-utils': 'preact/test-utils',
      'react-dom': 'preact/compat',
    },
    extensions: ['.js', '.jsx', '.json'],
    modules: [DEPENDENCIES_DIR],
  },
  externals: {
    perf_hooks: 'self',
  },
});
