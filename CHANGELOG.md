# Changelog
All notable changes to this extension will be documented in this file.

## [2.8.1] - 2023-11-08
### Changed
- Removed proxy feature



## [2.8.0] - 2023-09-28
### Changed
- Replaced webext-sdk with webext-ad-filtering-solution version 1.0.0
- Added auto-updates for easyprivacy list

## [2.7.1] - 2023-08-11
### Changed
- Updated crumbs-ext-sdk to fix cookie parse error when called with empty string (CRUMBS-2774)
- Fixed Sentry source map issues


## [2.7.0] - 2023-06-27
### Changed
- Using new PAT SDK v1.0.1
- Added Sentry to service worker, options page and popup UI
- Updated DNR rules
- Automated tests


## [2.6.0] - 2023-03-29
### Changed
- Integrated Crumbs Extension SDK 0.0.4


## [2.5.0] - 2023-03-07
### Changed
- Integrated Crumbs Extension SDK 0.0.3

## [2.4.1] - 2023-02-16
### Changed
- Fixed proxy logic. Previously, ad requests might not be proxied if there was a conflict with another extension.


## [2.4.0] - 2023-02-09
### Changed
- UI reorganization
- Fingerprint Shield performance optimizations and bug fixes
- Fixed popup issues for about:blank URLS
- Performance improvement for URL map


## [2.3.0] - 2022-12-09
### Changed
- Major refactoring
- Support for Manifest V3
- Fixed Manifest V2 to V3 migration
- Replaced Cookie Sandboxing with Cookie Expiration
- Added feature flag to block all third-party cookies
- Temporary share dummy data
- Fixed cookie decode error while parsing


## [2.2.1-beta] - 2022-12-08
### Changed
- Added feature flag to block all third-party cookies


## [2.2.0-beta] - 2022-11-29
### Changed
- Added expire cookies functionality
- Fixed anti-fingerprinting issues with WebGL2


## [2.1.4-beta] - 2022-11-21
### Changed
- Improved toDataURL and toBlob methods in anti-fingerprinting script
- Fixed onCommitted event signature
- Catch generic errors


## [2.1.0-beta] - 2022-09-22
### Changed
- Added fingerpriting protection


## [2.0.0-beta] - 2022-09-08
### Changed
- Manifest V3 support
- Removed cookie sandboxing
- Removed refferer trimming


## [1.11.0] - 2022-07-04
### Changed
- Updated webext-sdk to 0.4.1
- Extended the list of blocked query parameters


## [1.10.0] - 2022-05-26
### Changed
- Migrated from country names to country codes
- Regularly fetch safe-to-share list
- Use the safe-to-share list to decrease the probability of re-identifing the users


## [1.9.8] - 2022-05-09
### Changed
- Fixed issue with trackers blocked badge
- Wait for loading to complete before checking allow-listing


## [1.9.7] - 2022-03-10
### Changed
- Replace adblockpluscore with webext-sdk


## [1.9.6] - 2022-03-10
### Changed
- Added pt_BR chomestore description


## [1.9.5] - 2022-03-10
### Changed
- Updated telemetry schema


## [1.9.4] - 2022-03-10
### Changed
- Changed extension name


## [1.9.3] - 2022-01-18
### Changed
- New license


## [1.9.2] - 2021-12-14
### Changed
- New form for the uninstall process
- New form for issue reporting
- GPC signal enabled by default


## [1.9.1] - 2021-11-03
### Changed
- Fixed bug on GMail Calendar widget
- Fixed navigator.doNotTrack type
- Fixed pop-up layout on the overflow menu on Firefox
- Other minor bug fixes and improvements


## [1.9.0] - 2021-10-04
### Changed
- Added localStorage sandboxing
- Added missing German translations
- Minor bug fixes and improvements


## [1.8.7] - 2021-09-08
### Changed
- Added German translations
- Updated telemetry endpoint and authorization keys
- Fixed filter lists caching mechanism when updating
- Minor bug fixes and improvements


## [1.8.6] - 2021-08-09
### Changed
- Implemented differential privacy for the telemetry events
- Minor bug fixes and improvements


## [1.8.5] - 2021-07-19
### Changed
- Small change of the name of the extension


## [1.8.4] - 2021-07-07
### Changed
- Added notice for when proxy settings is already in use
- Upsell Crumbs Email Relay in the popup UI, if not activated
- Minor bug fixes and improvements


## [1.8.3] - 2021-06-17
### Changed
- Added anonymous diagnostics and usage data
- Minor bug fixes and improvements


## [1.8.2] - 2021-06-02
### Changed
- Improved Crumbs interest page to visually showcase interest relevance
- Simpler explanation videos for a better email relay experience
- Minor bug fixes and improvements


## [1.8.1] - 2021-05-19
### Changed
- Added update page
- Added mechanism to auto-update the proxy domains list
- Integrated a new API for Crumbs Email Relay
- Minor bug fixes and improvements


## [1.8.0] - 2021-04-22
### Changed
- Block Google's new mechanism for interest-based ad selection (FLoC)
- Added context-menu entry for Crumbs Email Relay
- Minor bug fixes and improvements


## [1.7.0] - 2021-04-07
### Changed
- Renamed Insights into Interests
- Decay Interests with 5% every 24h
- Send app version in the uninstall form


## [1.6.2] - 2021-03-15
### Changed
- Fixed the device and language insights
- Display app version in the settings page
- Added DIRECT fallback for ad requests proxy
- Fixed email relay in-page menu visibility
- Removed health related interests


## [1.6.1] - 2021-03-03
### Changed
- Insert local alias to domain associations in email relay dashboard
- Save locally all aliases generated for a specific domain

### Fixed:
- Settings page menu hover effect
- Prevent re-rendering of email relay in-page menu


## [1.6.0] - 2021-02-11
### Changed:
- Integrated Crumbs Email Relay
- Increased meta dependency version from 3.0.0 to 3.0.1
- Cleanup old interest ids that are no longer used
- Keep in memory only a Set on interests ids rather than the entire data
- Incremented the version of adblockpluscore to 3.10.1
- Replace endpoint for downloading the easylist cookielist

### Fixed
- Updated copyright year


## [1.5.5] - 2021-01-22
### Changed:
- Added license file
- Reset badge counter on each tab navigation
- Subscribed to privacy friendly allow list


## [1.5.4] - 2021-01-05
### Changed:
- Share insights with crumbs.org (only) over X-Crumbs-Insights response header


## [1.5.3] - 2020-12-10
### Changed:
- Small copy changes
- Increased minimum Gecko version from 57.0 to 63.0
- Added 2 seconds timeout for opening successfully installed page


## [1.5.2] - skipped


## [1.5.1] - 2020-11-25
### Changed:
- Start versioning
- Updated copy text
- Small refactoring
- Sanitise `proxy-domains.json` list

### Fixed:
- Do not intercept requests from `chrome:` and `about:` pages
- Make sure `domains.json` is fetched only once, in case of concurrency
