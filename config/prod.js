export const CRUMBS_WEBSITE_URL = 'https://crumbs.org';
export const CRUMBS_RELAY_URL = 'https://relay.crumbs.org';
export const CRUMBS_RELAY_API_URL = 'https://api-relay.crumbs.org';

export const TELEMETRY_DSN = 'https://crumbs.telemetry.eyeo.com/topic/crumbs_event/version/3';
