export const DEBUG = true;

export const CRUMBS_WEBSITE_URL = 'https://stage.crumbs.org';
export const CRUMBS_RELAY_URL = 'https://stage-relay.crumbs.org';
export const CRUMBS_RELAY_API_URL = 'https://api-stage-relay.crumbs.org';

export const TELEMETRY_DSN = 'https://test-telemetry.data.eyeo.it/topic/crumbs_event/version/3';
export const TELEMETRY_LOG_LEVEL = 'info';
