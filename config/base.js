/**
 * If true, PatSDK will log debug messages to console and Telemetry will not send events to server,
 * but rather log them.
 *
 * @type {boolean}
 */
export const DEBUG = false;

/**
 *
 * @type {string}
 */
export const TELEMETRY_LOG_LEVEL = 'error';
