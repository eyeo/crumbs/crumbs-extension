const baseConfig = require('./base');
const prodConfig = require('./prod');
const devConfig = require('./dev');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  ...baseConfig,
  ...(isProduction ? prodConfig : devConfig),
};
