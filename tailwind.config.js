// eslint-disable-file max-len
const customForms = require('@tailwindcss/forms');


module.exports = {
  content: ['./app/**/*.{html,jsx}'],
  theme: {
    extend: {
      colors: {
        black: '#000000',
        tan: '#CCB1AB',
        red: '#FF4F06',
        yellow: '#FFB40C',
        brown: '#66380F',
        'red-light': '#FFDDCF',
        'red-notice': '#8b0000',
        blue: {
          1: '#1E35F5',
          2: '#3E53D2',
        },
        gray: {
          1: '#E7EAF5',
          2: '#BBC3DA',
          3: '#AEB6D0',
          4: '#989FB2',
          5: '#505C6B',
          6: '#E8EBF2',
          7: '#F9FAFB',
        },
        'magenta-light': 'rgba(231, 234, 245, 0.6)',
        'blue-light': 'rgba(62, 83, 210, 0.8)',
        'blue-light-2': 'rgba(62, 83, 210, 0.25)',
        green: '#00C49A',
        'green-light': '#E1F5F2',
      },
      backgroundImage: {
        'email-relay': 'url(../icons/EmailRelayPopupBg.png)',
      },
      borderRadius: {
        xs: '3px',
        sm: '5px',
        lg: '10px',
      },
    },
    gradients: {
      'blue-radial': 'radial-gradient(130% 134.69% at 100% -15.71%, #1E35F5 0%, #3E53D2 100%)',
      'blue-linear': 'linear-gradient(90.03deg, #5374FF -101.32%, #6A87FF 99.97%)',
      'red-linear': 'linear-gradient(90.03deg, #DF694F -101.32%, #DF694F 99.97%)',
    },
    fontFamily: {
      body: ['CeraPro', 'Roboto', 'sans-serif'],
    },
    transitionProperty: {
      height: 'height',
    },
  },
  variants: {
    extend: {
      backgroundImage: ['checked'],
    },
  },
  plugins: [
    // add gradients as "background" css-properties
    ({ addUtilities, e, theme }) => {
      const gradients = Object.entries(theme('gradients', {}));
      const utilities = {};
      gradients.forEach(([key, value]) => {
        utilities[`.bg-${e(key)}`] = {
          background: value,
        };
      });
      addUtilities(utilities);
    },
    customForms,
  ],
};
