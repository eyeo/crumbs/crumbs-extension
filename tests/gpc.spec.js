import { test, expect, acceptTOS, checkTestsFromPage } from './fixtures';

test.describe('GPC', () => {
  test.beforeEach(async ({ context }) => {
    await acceptTOS(context);
  });

  test('Feature active - navigator property & header', async ({ page }) => {
    await page.goto('https://test-pages.crumbs.org/dom/gpc', { waitUntil: 'load' });
    await checkTestsFromPage(page);

    await page.goto('https://test-pages.crumbs.org/headers/set-headers', { waitUntil: 'load' });
    const locator = page.locator('tr', { hasText: 'Sec-GPC' }).locator('.TestResult');
    await expect(locator).toHaveAttribute('data-result', 'Passed', { timeout: 5000 });
  });

  test('Feature inactive - navigator property & header', async ({ page, context, extensionId }) => {
    // deactivate feature
    const optionsPage = await context.newPage();
    await optionsPage.goto(`chrome-extension://${extensionId}/options.html?path=settings`, { waitUntil: 'load' });

    await optionsPage.locator('#gpc').waitFor({ state: 'visible' });
    await optionsPage.locator('#gpc').uncheck();
    await optionsPage.close();

    await page.goto('https://test-pages.crumbs.org/dom/gpc', { waitUntil: 'load' });
    await checkTestsFromPage(page, 'Failed');

    await page.goto('https://test-pages.crumbs.org/headers/set-headers', { waitUntil: 'load' });
    const locator = page.locator('tr', { hasText: 'Sec-GPC' }).locator('.TestResult');
    await expect(locator).toHaveAttribute('data-result', 'Failed', { timeout: 5000 });
  });
});
