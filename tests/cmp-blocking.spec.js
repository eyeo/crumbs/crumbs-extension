import { test, acceptTOS, checkTestsFromPage } from './fixtures';

test.describe('CMP blocking', () => {
  test.beforeEach(async ({ context }) => {
    await acceptTOS(context);
  });

  test('Feature active', async ({ page }) => {
    await page.goto('https://test-pages.crumbs.org/filtering/cmp', { waitUntil: 'networkidle' });
    await checkTestsFromPage(page);
  });

  test('Feature inactive', async ({ page, context, extensionId }) => {
    // deactivate feature
    const optionsPage = await context.newPage();
    await optionsPage.goto(`chrome-extension://${extensionId}/options.html?path=settings`, { waitUntil: 'load' });

    await optionsPage.locator('#block_cmps').waitFor({ state: 'visible' });
    await optionsPage.locator('#block_cmps').uncheck();
    await optionsPage.close();

    await page.goto('https://test-pages.crumbs.org/filtering/cmp', { waitUntil: 'networkidle' });
    await checkTestsFromPage(page, 'Failed');
  });
});
