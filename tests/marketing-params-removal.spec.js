import { test, acceptTOS, checkTestsFromPage } from './fixtures';


test.describe('Marketing params filtering', () => {
  test.beforeEach(async ({ context }) => {
    await acceptTOS(context);
  });

  test('Feature active', async ({ page }) => {
    await page.goto('https://test-pages.crumbs.org/filtering/utm', { waitUntil: 'networkidle' });

    const runTestBtn = page.locator('button', { hasText: 'Run test' });
    await runTestBtn.click();

    await checkTestsFromPage(page);
  });

  test('Feature inactive', async ({ page, context, extensionId }) => {
    // deactivate feature
    const optionsPage = await context.newPage();
    await optionsPage.goto(`chrome-extension://${extensionId}/options.html?path=settings`, { waitUntil: 'load' });

    await optionsPage.locator('#remove_utm_params').waitFor({ state: 'visible' });
    await optionsPage.locator('#remove_utm_params').uncheck();
    await optionsPage.close();

    await page.goto('https://test-pages.crumbs.org/filtering/utm', { waitUntil: 'networkidle' });
    const runTestBtn = page.locator('button', { hasText: 'Run test' });
    await runTestBtn.click();

    await checkTestsFromPage(page, 'Failed');
  });
});

