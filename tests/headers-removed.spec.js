import { test, acceptTOS, checkTestsFromPage } from './fixtures';

test.beforeEach(async ({ context }) => {
  await acceptTOS(context);
});

test('Fingerpriting headers removing', async ({ page }) => {
  await page.goto('https://test-pages.crumbs.org/headers/removed-headers/', { waitUntil: 'networkidle' });
  await checkTestsFromPage(page);
});
