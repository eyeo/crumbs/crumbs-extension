import { test, acceptTOS, checkTestsFromPage } from './fixtures';

test.beforeEach(async ({ context }) => {
  await acceptTOS(context);
});

test('Trackers blocking', async ({ page }) => {
  await page.goto('https://test-pages.crumbs.org/filtering/network-requests', { waitUntil: 'networkidle' });
  await checkTestsFromPage(page);
});
