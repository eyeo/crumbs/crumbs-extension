import { test, expect, acceptTOS, checkTestsFromPage } from './fixtures';

test.describe('3rd party cookie expiration', () => {
  test.beforeEach(async ({ context }) => {
    await acceptTOS(context);
  });

  test('Feature active', async ({ page }) => {
    await page.goto('https://test-pages.crumbs.org/cookies/cookies-expiration', { waitUntil: 'load' });
    const locator = page.locator('li', { hasText: 'Check after 120 seconds' });
    await expect(locator.locator('span')).toHaveText('✅', { timeout: 130000 });
    await checkTestsFromPage(page);
  });

  test('Feature inactive', async ({ page, context, extensionId }) => {
    // deactivate feature
    const optionsPage = await context.newPage();
    await optionsPage.goto(`chrome-extension://${extensionId}/options.html?path=settings`, { waitUntil: 'load' });

    await optionsPage.locator('#block_3rd_cookies').waitFor({ state: 'visible' });
    await optionsPage.locator('#block_3rd_cookies').uncheck();
    await optionsPage.close();

    await page.goto('https://test-pages.crumbs.org/cookies/cookies-expiration', { waitUntil: 'load' });
    const locator = page.locator('li', { hasText: 'Check after 120 seconds' });
    await expect(locator.locator('span')).toHaveText('✅', { timeout: 130000 });


    const failedTests = page.locator('table')
      .nth(1)
      .or(page.locator('table').nth(3))
      .locator('tr', { hasText: 'over-1m' });
    await expect(failedTests).toHaveCount(3, { timeout: 5000 });

    const allTests = await page.locator('tbody > tr').all();
    await expect(allTests.length).toBeTruthy();

    for (const testLocator of allTests) {
      const testResult = testLocator.locator('.TestResult');
      // eslint-disable-next-line no-await-in-loop
      const isFailed = await failedTests.filter({ hasText: await testLocator.textContent() }).count();
      const expected = isFailed ? 'Failed' : 'Passed';
      // eslint-disable-next-line no-await-in-loop
      await expect(testResult).toHaveAttribute('data-result', expected, { timeout: 5000 });
    }
  });
});

