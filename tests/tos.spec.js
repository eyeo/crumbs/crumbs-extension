import { test, expect, acceptTOS } from './fixtures';

test('Accepting TOS from crumbs.org', async ({ context, extensionId }) => {
  // Since it is fresh instance of a browser and extension is just installed,
  // it should have opened crumbs.org/thank-you where the TOS popup is displayed
  await acceptTOS(context);

  // check if extension is initialized
  const popupPage = await context.newPage();
  await popupPage.goto(`chrome-extension://${extensionId}/popup.html`, { waitUntil: 'networkidle' });
  await expect(popupPage.getByText('Found an issue?')).toBeAttached();
  await expect(popupPage.getByText('Cookies Blocked')).toBeAttached();
});

test('Accepting TOS from extension popup', async ({ page, extensionId }) => {
  await page.goto(`chrome-extension://${extensionId}/popup.html`, { waitUntil: 'networkidle' });
  await page.locator('button').click(); // accept TOS

  // check if extension is initialized
  await expect(page.getByText('Found an issue?')).toBeAttached();
  await expect(page.getByText('Cookies Blocked')).toBeAttached();
});
