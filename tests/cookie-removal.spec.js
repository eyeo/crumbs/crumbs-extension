import { test, acceptTOS, checkTestsFromPage } from './fixtures';

test.describe('3rd party cookie removal', () => {
  test.beforeEach(async ({ context }) => {
    await acceptTOS(context);
  });

  test('Feature active', async ({ context, page, extensionId }) => {
    // activate features
    const optionsPage = await context.newPage();
    await optionsPage.goto(`chrome-extension://${extensionId}/options.html?path=settings`, { waitUntil: 'load' });
    await optionsPage.locator('.form-select').waitFor({ state: 'visible' });
    await optionsPage.locator('.form-select').selectOption('2'); // select 3rd party cookie removal;
    await optionsPage.close();

    await page.goto('https://test-pages.crumbs.org/cookies/cookies-removal', { waitUntil: 'load' });
    await checkTestsFromPage(page.locator('main'));
  });

  test('Feature inactive', async ({ context, page, extensionId }) => {
    // deactivate feature
    const optionsPage = await context.newPage();
    await optionsPage.goto(`chrome-extension://${extensionId}/options.html?path=settings`, { waitUntil: 'load' });

    await optionsPage.locator('#block_3rd_cookies').waitFor({ state: 'visible' });
    await optionsPage.locator('#block_3rd_cookies').uncheck();
    await optionsPage.close();

    await page.goto('https://test-pages.crumbs.org/cookies/cookies-removal', { waitUntil: 'load' });
    const tableLocators = await page.locator('table').all();
    for (const tableLocator of tableLocators) {
      const expected = tableLocators.indexOf(tableLocator) % 2 ? 'Failed' : 'Passed';
      // eslint-disable-next-line no-await-in-loop
      await checkTestsFromPage(tableLocator, expected);
    }
  });
});
