import base from '@playwright/test';

export const acceptTOS = async (context) => {
  const tosPage = await context.waitForEvent('page');
  await tosPage.waitForLoadState('networkidle');

  // accept TOS
  const acceptBtn = tosPage.locator('button', { hasText: 'Agree' });
  await acceptBtn.click();

  // enable CMP blocking
  const cmpCheckbox = tosPage.locator('input[type="checkbox"]');
  await cmpCheckbox.check();
  const activateBtn = tosPage.locator('.ReactModal__Overlay button', { hasText: 'Activate' });
  await activateBtn.click();

  await tosPage.waitForTimeout(3000); // make sure extension has time to init
  await tosPage.close();
};

export const checkTestsFromPage = async (startingLocator, result = 'Passed') => {
  const locator = startingLocator.locator('.TestResult');
  const testResults = await locator.all();
  // make sure we have the elements on the page
  await base.expect(testResults.length).toBeTruthy();

  for (const testResult of testResults) {
    // eslint-disable-next-line no-await-in-loop
    await base.expect(testResult).toHaveAttribute('data-result', result, { timeout: 5000 });
  }
};

export const test = base.extend({
  context: async ({ playwright }, use) => {
    const args = [
      `--disable-extensions-except=${__dirname}/../dist`,
      `--load-extension=${__dirname}/../dist`,
    ];
    if (process.env.CI) { // on CI run headless
      args.push('--headless=new');
    }

    const context = await playwright.chromium.launchPersistentContext('', {
      headless: false,
      args,
    });

    await use(context);
    await context.close();
  },
  extensionId: async ({ context }, use) => {
    /*
    // for manifest v2:
    let [background] = context.backgroundPages()
    if (!background)
      background = await context.waitForEvent('backgroundpage')
    */

    // for manifest v3:
    let [background] = context.serviceWorkers();
    if (!background) {
      background = await context.waitForEvent('serviceworker');
    }

    const extensionId = background.url().split('/')[2];
    await use(extensionId);
  },
});


export const { expect } = base;
