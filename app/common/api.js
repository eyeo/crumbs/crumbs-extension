// Open long-lived communications with background page
const port = browser.runtime.connect({ name: 'ui' });

const portListeners = new Map();
port.onMessage.addListener((message) => {
  const { type } = message;

  if (portListeners.has(type)) {
    for (const listener of portListeners.get(type)) {
      listener(message);
    }
  }
});

/**
 * Tracks changes for given event name and calls the callback listener
 *
 * @param eventName
 * @param listener
 */
port.on = (eventName, listener) => {
  if (portListeners.has(eventName)) {
    const listeners = portListeners.get(eventName);
    listeners.push(listener);
  } else {
    portListeners.set(eventName, new Set([listener]));
  }
};

/**
 * Removed changes listener for given event name
 *
 * @param eventName
 * @param listener
 */
port.off = (eventName, listener) => {
  if (portListeners.has(eventName)) {
    const listeners = portListeners.get(eventName);
    listeners.delete(listener);
  }
};


const tabs = {
  /**
   * Retrieves the current active tab
   */
  getCurrent: new Promise(
    (resolve) => {
      document.addEventListener('DOMContentLoaded', () => {
        browser.tabs.query({ active: true, currentWindow: true })
          .then((queryTabs) => {
            resolve({ id: queryTabs[0].id, url: queryTabs[0].url });
          });
      });
    },
  ),

  /**
   * Refresh tab.
   *
   * @param tab to be refreshed
   * @returns {Promise<*>}
   */
  refresh: async (tab) => browser.tabs.reload(tab.id),

  /**
   * Checks if a given tab url is in the allowlist.
   *
   * @param tab to be checked
   * @returns {Promise<*>}
   */
  isAllowed: async (tab) => browser.runtime.sendMessage({
    type: 'filters.isAllowed',
    tab,
  }),

  /**
   * Disables the tracking protection for the given tab.
   *
   * @param tab
   * @returns {Promise<*>}
   */
  allow: async (tab) => browser.runtime.sendMessage({
    type: 'filters.allow',
    tab,
  }),

  /**
   * Enables the tracking protection for the given tab.
   *
   * @param tab
   * @returns {Promise<*>}
   */
  block: async (tab) => browser.runtime.sendMessage({
    type: 'filters.block',
    tab,
  }),
};

const stats = {
  /**
   * Gets the latest stats total and for current page.
   * e.g. {totalTrackers: 2651, page: {trackers: 50, cookies: 50}}
   *
   * @param tab
   * @returns {Promise<*>}
   */
  get: async (tab) => browser.runtime.sendMessage({
    type: 'crumbs.getStats',
    tab,
  }),

  /**
   * Watches for stats changes, and calls the callback with updates stats object.
   *
   * @param tab
   * @param listener
   */
  watch: (tab, listener) => {
    const wrapper = () => stats.get(tab).then(listener);
    port.on('crumbs.statsChanged', wrapper);
  },

  /**
   * Removes the watch listener.
   *
   * @param listener
   */
  unwatch: (listener) => {
    port.off('crumbs.statsChanged', listener);
  },
};


const settings = {
  /**
   * Gets the latest settings flags.
   *
   * @returns {Promise<*>}
   */
  get: async () => browser.runtime.sendMessage({
    type: 'settings.get',
  }),

  /**
   * Updates the settings flag by name.
   *
   * @param name
   * @param value
   * @returns {Promise<*>}
   */
  set: async (name, value) => browser.runtime.sendMessage({
    type: 'settings.set',
    name,
    value,
  }),

  /**
   * Opens a new tab with the settings or insights page active
   *
   * @param path {string<settings|insights>}
   * @returns {Promise<void>}
   */
  openOptionsPage: async (path = 'settings') => {
    const url = browser.runtime.getURL('options.html');
    const finalUrl = `${url}?path=${path}`;
    const allTabs = await browser.tabs.query({});
    const existingTab = allTabs.filter((tab) => tab.url.indexOf(url) === 0)[0];

    // If there is a tab with the options page already open, focus on that window and page.
    if (existingTab) {
      const newWindow = await browser.windows.get(existingTab.windowId);
      if (!newWindow.focused) {
        browser.windows.update(
          newWindow.id,
          { focused: true },
        );
      }

      browser.tabs.update(existingTab.id, { url: finalUrl, active: true }).then(() => window.close());
    } else {
      // Create a new tab with the options page if there isn't one already open.
      browser.tabs.create({
        active: true,
        url: finalUrl,
      }).then(() => window.close());
    }
  },
};

const insights = {
  /**
   * Get the full list of insights.
   *
   * @returns {Promise<*>}
   */
  all: async () => (
    await browser.runtime.sendMessage({
      type: 'crumbs.getInsights',
    }) || []
  ),

  /**
   * Get the list of shared insights for the given tab.
   *
   * @param tab
   * @returns {Promise<*>}
   */
  getShared: async (tab) => browser.runtime.sendMessage({
    type: 'crumbs.getSharedInsights',
    tab,
  }),

  /**
   * Disables an insight from sharing.
   *
   * @param insightId The id of the insight
   * @return {Promise<*>}
   */
  disable: async (insightId) => browser.runtime.sendMessage({
    type: 'crumbs.disableInsight',
    insightId,
  }),

  /**
   * Enables an insight from sharing.
   *
   * @param insightId The id of the insight
   * @return {Promise<*>}
   */
  enable: async (insightId) => browser.runtime.sendMessage({
    type: 'crumbs.enableInsight',
    insightId,
  }),
};

const relay = {
  /**
   * Generate a new alias
   *
   * @returns {Promise<null|*>}
   */
  generateAlias: async () => browser.runtime.sendMessage({
    type: 'relay.generateAlias',
  }),

  /**
   * Close in page menu
   */
  closeMenu: () => {
    // Important: use setTimeout otherwise FF blocks postMessage if it is sent in a onClick callback
    setTimeout(() => {
      window.parent.postMessage(
        {
          type: 'crumbs.menu.close',
        },
        '*',
      );
    }, 10);
  },

  /**
   * Fill in page input with given alias
   *
   * @param alias
   */
  fillInput: (alias) => {
    // Important: use setTimeout otherwise FF blocks postMessage if it is sent in a onClick callback
    setTimeout(() => {
      window.parent.postMessage(
        {
          type: 'crumbs.menu.fill',
          email: alias,
        },
        '*',
      );
    }, 10);

    // Updated new generated alias in context menu.
    browser.runtime.sendMessage({
      type: 'contextmenu.update_alias',
    });
  },

  getAssociatedAlias: async () => browser.runtime.sendMessage({
    type: 'relay.get',
  }),
};

const events = {
  sendEvent: async (type, data) => browser.runtime.sendMessage({
    type,
    ...data,
  }),
};

const api = {
  tabs,
  settings,
  stats,
  insights,
  relay,
  events,
};

export default api;
