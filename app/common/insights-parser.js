import CountryCodes from './codes-to-countries.json';


/**
 * Parses all the insights for display.
 *
 * @param {{ interests: [], demographics: [] }} insights
 * @returns {*[]}
 */
// eslint-disable-next-line import/prefer-default-export
export function parseInsights(insights) {
  const { interests, demographics } = insights;

  // convert location from country code to country name
  for (const item of demographics) {
    if (item.id === 'location') {
      item.name = CountryCodes[item.name] || item.name;
    }
  }

  return [
    ...demographics,
    ...interests,
  ];
}
