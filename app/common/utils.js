/**
 * Formats given number into human readable
 * e.g. 24K, 1M...
 *
 * @param n {number}
 * @returns {string}
 */
function prettifyNumber(n) {
  return new Intl.NumberFormat('en-GB', {
    notation: 'compact',
    compactDisplay: 'short',
  }).format(n);
}


export {
  // eslint-disable-next-line import/prefer-default-export
  prettifyNumber,
};
