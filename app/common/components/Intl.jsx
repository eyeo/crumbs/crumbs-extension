import { createElement } from 'preact';
import PropTypes from 'prop-types';

const Intl = ({
  name, nodeType = 'span', params = [],
  ...otherProps
}) => {
  const str = browser.i18n.getMessage(name, params);
  return createElement(nodeType, {
    ...otherProps,
    dangerouslySetInnerHTML: {
      __html: str,
    },
  });
};

Intl.propTypes = {
  name: PropTypes.string.isRequired,
  nodeType: PropTypes.string,
  params: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
  ),
};

Intl.defaultProps = {
  nodeType: 'span',
  params: [],
};


export default Intl;
