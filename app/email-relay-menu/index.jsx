import { h, render } from 'preact';
import EmailMenu from './EmailMenu';
import '../assets/css/main.css';

render(<EmailMenu />, document.getElementById('app'));
