import { h } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import PropTypes from 'prop-types';
import api from '../common/api';
import { CRUMBS_RELAY_URL } from '../../config';
import Intl from '../common/components/Intl';


function GenerateAliasBtn({ onClick }) {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(async () => {
    const settings = await api.settings.get();
    setIsAuthenticated(!!settings.api_key);
  }, []);

  const cssClasses = `
    btn bg-green text-white block text-center font-bold
    p-2 rounded-sm cursor-pointer outline-none focus:outline-none
    hover:bg-opacity-75 focus:bg-opacity-75 active:bg-opacity-75
    transition duration-300 ease-in-out
  `;
  const title = <Intl name="options_relay_generate" />;

  if (!isAuthenticated) {
    return (
      <a
        onClick={() => { api.relay.closeMenu(); return true; }}
        href={`${CRUMBS_RELAY_URL}/account/profile/`}
        className={cssClasses}
        rel="noopener noreferrer"
        target="_blank"
      >
        {title}
      </a>
    );
  }

  return (
    <button
      type="button"
      onClick={onClick}
      className={cssClasses}
    >
      {title}
    </button>
  );
}

GenerateAliasBtn.propTypes = {
  onClick: PropTypes.func,
};

GenerateAliasBtn.defaultProps = {
  onClick: () => {},
};


export default function EmailMenu() {
  const [associatedAlias, setAlias] = useState(null);

  useEffect(async () => {
    const alias = await api.relay.getAssociatedAlias();
    setAlias(alias);
  }, []);

  const onClickGenerateAlias = async (event) => {
    event.preventDefault();
    const alias = await api.relay.generateAlias();
    if (alias) {
      api.relay.fillInput(alias);
    }
  };

  let association = (
    <div className="pt-2 pb-2 w-full text-center text-gray-4">
      <Intl name="options_relay_no_aliases" />
    </div>
  );

  if (associatedAlias) {
    association = (
      <button
        type="button"
        className="bg-gray-1 rounded-sm p-2 outline-none focus:outline-none hover:bg-gray-2 w-full"
        onClick={() => { api.relay.fillInput(associatedAlias); }}
      >
        { associatedAlias }
      </button>
    );
  }

  return (
    <div className="bg-white flex flex-col items-center rounded-lg text-sm space-y-3">
      <div className="w-full pt-2 pl-2 pr-2">
        { association }
      </div>
      <div>
        <GenerateAliasBtn onClick={onClickGenerateAlias} />
      </div>
      <div className="bg-gray-7 w-full flex justify-center rounded-b-lg">
        <a
          onClick={() => { api.relay.closeMenu(); return true; }}
          href={`${CRUMBS_RELAY_URL}/account/profile/`}
          className="text-green cursor-pointer no-underline p-2"
          rel="noopener noreferrer"
          target="_blank"
        >
          <Intl name="options_relay_manage" />
        </a>
      </div>
    </div>
  );
}
