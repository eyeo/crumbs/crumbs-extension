import { h } from 'preact';
import PropTypes from 'prop-types';
import PlayButton from './PlayButton';
import ProtectionInfo from './ProtectionInfo';
import RefreshButton from './RefreshButton';
// import Countdown from './Countdown';

export default function TrackerProtectionControls(props) {
  const {
    className, variant, changed, domain, onChange, onRefresh,
  } = props;

  return (
    <div
      className={`
        ${variant === 'inactive' ? 'bg-red' : 'bg-green'} ${className}
        rounded-sm px-2 flex flex-col justify-center
        transition-colors duration-300 ease-in-out
      `}
      style={{ height: 50 }}
    >
      <div className="flex justify-between items-center">
        <div className="flex space-x-3">
          <PlayButton
            variant={variant}
            onClick={() => { onChange(variant !== 'active'); }}
          />
          <ProtectionInfo variant={variant} domain={domain} />
        </div>
        <div>
          {/* { variant === 'inactive' && !changed && <Countdown /> } */}
          { changed
            && (
            <RefreshButton
              variant={variant}
              onClick={onRefresh}
            />
            )}
        </div>
      </div>
    </div>
  );
}

TrackerProtectionControls.propTypes = {
  className: PropTypes.string,
  domain: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['active', 'inactive']),
  changed: PropTypes.bool,
  onChange: PropTypes.func,
  onRefresh: PropTypes.func,
};

TrackerProtectionControls.defaultProps = {
  className: '',
  variant: 'active',
  changed: false,
  onChange: () => {},
  onRefresh: () => {},
};
