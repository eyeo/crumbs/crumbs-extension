import { h, Fragment } from 'preact';
import { CRUMBS_RELAY_URL } from '../../../config';
import api from '../../common/api';
import Intl from '../../common/components/Intl';

export default function EmailRelayUpsell() {
  const onRelayClick = async (event) => {
    event.stopPropagation();
    await api.events.sendEvent('popup.upsell', { feature: 'relay' });
    browser.tabs.create({
      active: true,
      url: CRUMBS_RELAY_URL,
    }).then(() => window.close());
  };

  return (
    <Fragment>
      <div
        className="mb-3 p-3 relative text-brown text-xs rounded bg-email-relay bg-contain"
        style={{
          height: 135,
        }}
      >
        <Intl
          name="popup_relay_upsell_title"
          nodeType="div"
          className="block mb-6 text-base font-bold"
          style={{
            maxWidth: 225,
          }}
        />
        <button type="button" onClick={onRelayClick} className="inline-block px-3 py-2 rounded bg-yellow">
          <Intl name="popup_relay_upsell_activate" className="text-sm" />
        </button>
      </div>
    </Fragment>
  );
}
