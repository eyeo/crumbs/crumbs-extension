import { h } from 'preact';
import SnoozeIcon from '../../assets/img/SnoozeIcon.svg';

function Countdown() {
  return (
    <div className="flex space-x-1 items-center">
      <SnoozeIcon />
      <span className="text-white font-bold text-lg">32:01</span>
    </div>
  );
}

export default Countdown;
