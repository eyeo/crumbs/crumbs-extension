import { h } from 'preact';
import PropTypes from 'prop-types';

import api from '../../common/api';
import Intl from '../../common/components/Intl';
import CrumbsLogo from '../../assets/img/CrumbsFullLogo.svg';
import GearIcon from '../../assets/img/GearIcon.svg';
import { CRUMBS_WEBSITE_URL } from '../../../config';


export default function Header(props) {
  const { className, variant, showRelayFeature } = props;

  const onClick = (event) => {
    event.stopPropagation();
    api.settings.openOptionsPage();
  };

  const onReportIssueClick = async (event) => {
    event.stopPropagation();
    const reportIssueURL = new URL(`${CRUMBS_WEBSITE_URL}/report-issue/?version=${process.env.ADDON_VERSION}`);

    // get url of the active tab
    const tabs = await browser.tabs.query({ active: true, currentWindow: true });
    if (tabs.length > 0) {
      reportIssueURL.searchParams.set('url', tabs[0].url);
    }

    await browser.tabs.create({
      active: true,
      url: reportIssueURL.toString(),
    });
  };


  return (
    <div className={`${className} text-white flex justify-between`}>
      <span className={`${variant === 'active' ? 'text-green' : 'text-red'}
                       transition-colors duration-200 ease-in-out`}
      >
        <CrumbsLogo style={{ height: 30 }} />
      </span>
      <div className="flex items-center">
        <button
          type="button"
          onClick={onReportIssueClick}
          className={`
            text-sm text-black hover:opacity-75 mr-5 focus:outline-none
            transition-opacity duration-200 ease-in-out
          `}
        >
          <Intl name="popup_found_issue" />
        </button>
        <button
          type="button"
          className={
            `hover:text-gray-5 focus:outline-none text-black transition-colors
            duration-200 ease-in-out flex justify-center`
          }
          onClick={onClick}
        >
          <GearIcon className="fill-current" />
          {
            showRelayFeature && (
              <span className="flex h-2 w-2 relative -mr-2">
                <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-yellow opacity-75" />
                <span className="relative inline-flex rounded-full h-2 w-2 bg-yellow" />
              </span>
            )
          }
        </button>
      </div>
    </div>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.string,
  showRelayFeature: PropTypes.bool,
};

Header.defaultProps = {
  className: '',
  variant: 'active',
  showRelayFeature: false,
};
