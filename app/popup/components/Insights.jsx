import { h, Fragment } from 'preact';
import PropTypes from 'prop-types';
import Interest from './common/Interest';
import NoCrumbs from '../../assets/img/NoCrumbs.svg';
import api from '../../common/api';
import Intl from '../../common/components/Intl';

function SharedInterests(props) {
  const { currentDomain, interests } = props;
  return (
    <Fragment>
      <Intl
        name="popup_interests_shared"
        nodeType="div"
        params={[currentDomain]}
        className="mb-3 text-xs truncate"
      />
      <div className="flex flex-wrap -mx-1">
        {
          interests.map((interest) => (
            <div key={interest.id} className="px-1 mb-2">
              <Interest name={interest.name} emoji={interest.emoji} />
            </div>
          ))
        }
      </div>
    </Fragment>
  );
}

SharedInterests.propTypes = {
  currentDomain: PropTypes.string,
  interests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      emoji: PropTypes.string,
      enabled: PropTypes.bool,
    }),
  ).isRequired,
};

SharedInterests.defaultProps = {
  currentDomain: 'this page',
};

function NoInterests() {
  return (
    <div className="flex flex-wrap justify-center mt-4 text-xs">
      <div className="mb-2">
        <NoCrumbs />
      </div>
      <Intl name="popup_no_interests_shared" nodeType="div" className="w-full text-center text-gray-4" />
    </div>
  );
}

export default function Insights(props) {
  const { className, currentDomain, interests } = props;

  const onClick = (event) => {
    event.stopPropagation();
    api.settings.openOptionsPage('insights');
  };

  return (
    <div className={className}>
      <div className="text-white flex justify-between">
        <Intl
          name="popup_crumbs_interests"
          nodeType="span"
          className="text-black font-bold text-base"
        />
        <button
          type="button"
          className={`
            text-green text-sm focus:outline-none
            hover:opacity-75 transition-opacity duration-200 ease-in-out
          `}
          onClick={onClick}
        >
          <Intl name="popup_see_all" />
        </button>
      </div>
      {
        interests && interests.length ? <SharedInterests currentDomain={currentDomain} interests={interests} />
          : <NoInterests />
      }
    </div>
  );
}

Insights.propTypes = {
  className: PropTypes.string,
  currentDomain: PropTypes.string,
  interests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      emoji: PropTypes.string,
      enabled: PropTypes.bool,
    }),
  ),
};

Insights.defaultProps = {
  className: '',
  currentDomain: 'this page',
  interests: undefined,
};
