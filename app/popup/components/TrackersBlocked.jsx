import { h } from 'preact';
import PropTypes from 'prop-types';
import LineChartSVG from '../../assets/img/LineChart.svg';
import Intl from '../../common/components/Intl';

export default function TrackersBlocked(props) {
  const { className, value } = props;

  return (
    <div
      className={`${className} bg-green-light p-3 rounded-sm`}
    >
      <LineChartSVG />
      <Intl name="popup_trackers_blocked" nodeType="div" className="font-bold mt-1 text-sm" />
      <div
        className="font-bold mt-1 text-2xl"
      >
        {value.toLocaleString()}
      </div>
      <div className="text-white mt-2 text-xs">
        <Intl
          name="popup_since_install"
          nodeType="span"
          className="bg-green px-2 rounded-sm text-white inline-block align-middle"
          style={{ height: 20, lineHeight: '20px', fontSize: 12 }}
        />
      </div>
    </div>
  );
}

TrackersBlocked.propTypes = {
  className: PropTypes.string,
  value: PropTypes.number.isRequired,
};

TrackersBlocked.defaultProps = {
  className: '',
};
