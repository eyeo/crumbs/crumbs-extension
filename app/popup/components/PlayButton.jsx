import { h } from 'preact';
import PropTypes from 'prop-types';
import PlayIcon from '../../assets/img/PlayIcon.svg';
import PauseIcon from '../../assets/img/PauseIcon.svg';

function PlayButton({ variant, onClick }) {
  const ButtonIcon = variant === 'inactive' ? PlayIcon : PauseIcon;
  return (
    <button
      type="button"
      onClick={onClick}
      className={`
        ${variant === 'inactive' ? 'bg-white' : 'bg-white'} rounded-full focus:outline-none
        hover:opacity-75 transition-opacity duration-200 ease-in-out
      `}
      style={{ width: 32, height: 32 }}
    >
      <ButtonIcon
        className={`fill-current ${variant === 'inactive' ? 'text-red' : 'text-green'} mx-auto`}
      />
    </button>
  );
}

PlayButton.propTypes = {
  variant: PropTypes.oneOf(['active', 'inactive']),
  onClick: PropTypes.func,
};

PlayButton.defaultProps = {
  variant: 'active',
  onClick: null,
};

export default PlayButton;
