import { h } from 'preact';
import PropTypes from 'prop-types';
import Intl from '../../common/components/Intl';

function ProtectionInfo({ domain, variant }) {
  return (
    <div className="flex flex-col">
      <Intl
        name="popup_protecting_domain"
        params={[domain]}
        className="font-bold uppercase text-opacity-75 truncate"
        style={{ fontSize: '0.5rem', maxWidth: 200 }}
      />
      <Intl
        name={`popup_protection_${variant === 'inactive' ? 'off' : 'on'}`}
        className="font-bold text-white"
        style={{ fontSize: '0.875rem' }}
      />
    </div>
  );
}

ProtectionInfo.propTypes = {
  domain: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['active', 'inactive']),
};

ProtectionInfo.defaultProps = {
  variant: 'active',
};

export default ProtectionInfo;
