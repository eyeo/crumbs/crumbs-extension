import { h } from 'preact';
import PropTypes from 'prop-types';

export default function Interest(props) {
  const {
    name, emoji, className,
  } = props;

  return (
    <div
      className={`${className} bg-magenta-light px-2 rounded-xs flex items-center`}
    >
      <span className="mr-1 text-base leading-6">{emoji}</span>
      <span className="text-black text-xs">{name}</span>
    </div>
  );
}

Interest.propTypes = {
  className: PropTypes.string,
  name: PropTypes.number.isRequired,
  emoji: PropTypes.string.isRequired,
};

Interest.defaultProps = {
  className: '',
};
