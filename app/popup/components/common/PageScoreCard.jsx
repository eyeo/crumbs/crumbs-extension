import { h } from 'preact';
import PropTypes from 'prop-types';
import { prettifyNumber } from '../../../common/utils';
import Intl from '../../../common/components/Intl';

export default function PageScoreCard(props) {
  const {
    className, variant = 'active', value, title,
  } = props;

  const scoreBackground = variant === 'active' ? 'bg-green' : 'bg-red';
  return (
    <div
      className={`${className} ${variant === 'inactive' ? 'bg-red-light' : 'bg-green-light'}
      flex space-x-2 px-2 content-center flex-wrap rounded-sm p-3`}
    >
      <div
        className={`
          ${scoreBackground} flex justify-center content-center flex-wrap text-white text-base rounded-sm
          transition-colors duration-300 ease-in-out
        `}
        style={{ width: 40, height: 40 }}
      >
        <span className="font-bold">{prettifyNumber(value)}</span>
      </div>
      <div className="flex flex-col justify-center" style={{ fontSize: 12 }}>
        <span className="font-bold text-black">
          {title}
        </span>
        <Intl
          name="popup_on_this_page"
          nodeType="span"
          className="text-gray-4"
        />
      </div>
    </div>
  );
}

PageScoreCard.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(['active', 'inactive']),
  value: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

PageScoreCard.defaultProps = {
  className: '',
  variant: 'active',
};
