import { h } from 'preact';
import PropTypes from 'prop-types';
import RefreshIcon from '../../assets/img/RefreshIcon.svg';
import Intl from '../../common/components/Intl';

function RefreshButton({ variant, onClick }) {
  return (
    <button
      type="button"
      onClick={onClick}
      className={`
        ${variant === 'inactive' ? 'text-red' : 'text-green'}
        bg-white rounded-sm px-2 flex space-x-1 items-center focus:outline-none
        hover:bg-opacity-75 transition-colors duration-200 ease-in-out
      `}
      style={{ height: 26, fontSize: '0.875rem' }}
    >
      <RefreshIcon className="fill-current" />
      <Intl name="popup_refresh_button" />
    </button>
  );
}

RefreshButton.propTypes = {
  variant: PropTypes.oneOf(['active', 'inactive']),
  onClick: PropTypes.func,
};

RefreshButton.defaultProps = {
  variant: 'active',
  onClick: null,
};

export default RefreshButton;
