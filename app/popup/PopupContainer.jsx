import { h, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import PropTypes from 'prop-types';

import api from '../common/api';
import Header from './components/Header';
import TrackerProtectionControls from './components/TrackerProtectionControls';
import PageScoreCard from './components/common/PageScoreCard';
import TrackersBlocked from './components/TrackersBlocked';
import Insights from './components/Insights';
import EmailRelayUpsell from './components/EmailRelayUpsell';
import Intl from '../common/components/Intl';


export default function PopupContainer(props) {
  const {
    currentDomain, interests, isActive, stats: initialStats, tab, showRelayFeature,
  } = props;
  const [variant, setVariant] = useState(isActive ? 'active' : 'inactive');
  const [changed, setChanged] = useState(false);

  const [stats, setStats] = useState(initialStats);
  useEffect(() => {
    const changeListener = (data) => {
      setStats(data);
    };

    api.stats.watch(tab, changeListener);

    // clean up function for listener
    return () => {
      // remove listener
      api.stats.unwatch(changeListener);
    };
  }, []);

  const onRefresh = () => {
    api.tabs.refresh(tab);
    window.close();
  };

  const onChange = async (enabled) => {
    await (enabled ? api.tabs.block(tab) : api.tabs.allow(tab));
    setChanged(!changed);
    setVariant(enabled ? 'active' : 'inactive');
  };

  const getBottomContent = () => {
    const shouldUpsellEmail = (!interests || !interests.length) && showRelayFeature;

    if (shouldUpsellEmail) {
      return <EmailRelayUpsell />;
    }

    return <Insights interests={interests} currentDomain={currentDomain} />;
  };

  return (
    <Fragment>
      <Header className="mb-4" variant={variant} showRelayFeature={showRelayFeature} />
      {currentDomain && (
        <TrackerProtectionControls
          className="mb-3"
          domain={currentDomain}
          variant={variant}
          changed={changed}
          onRefresh={onRefresh}
          onChange={onChange}
        />
      )}
      <div className="flex mb-4 items-stretch justify-between">
        <TrackersBlocked className="" value={stats.totalTrackers} />
        <div className="flex flex-col ml-3 justify-between flex-grow">
          <PageScoreCard title={<Intl name="popup_trackers_blocked" />} value={stats.page.trackers} variant={variant} />
          <PageScoreCard title={<Intl name="popup_cookies_blocked" />} value={stats.page.cookies} />
        </div>
      </div>
      {
        getBottomContent()
      }
      <div
        className="bg-green fixed bottom-0 left-0 w-full"
        style={{
          height: 3,
        }}
      />
    </Fragment>
  );
}

PopupContainer.propTypes = {
  tab: PropTypes.objectOf(PropTypes.string).isRequired,
  currentDomain: PropTypes.string,
  isActive: PropTypes.bool.isRequired,
  interests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      emoji: PropTypes.string,
      enabled: PropTypes.bool,
    }),
  ),
  stats: PropTypes.objectOf(PropTypes.string).isRequired,
  showRelayFeature: PropTypes.bool,
};

PopupContainer.defaultProps = {
  currentDomain: undefined,
  interests: undefined,
  showRelayFeature: false,
};
