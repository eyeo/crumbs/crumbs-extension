import { h, render } from 'preact';
import { useState } from 'preact/hooks';
import PropTypes from 'prop-types';
import * as Sentry from '@sentry/react';
import api from '../common/api';
import TOSContainer from './TOSContainer';
import PopupContainer from './PopupContainer';
import '../assets/css/main.css';
import { parseInsights } from '../common/insights-parser';


Sentry.init({
  dsn: process.env.SENTRY_DSN,
  tunnel: 'https://raven.crumbs.org',
  release: browser.runtime.getManifest().version,
  environment: process.env.ENV_NAME,
  tracesSampleRate: 0.1,
});


async function getData(tab) {
  const result = {
    interests: [],
  };

  result.stats = await api.stats.get(tab);
  result.isActive = !await api.tabs.isAllowed(tab);

  const interests = await api.insights.getShared(tab);
  if (interests) {
    result.interests = parseInsights(interests);
  }

  return result;
}


function App({
  data,
  tab,
  currentDomain,
}) {
  const [state, setState] = useState(data);

  const onAccept = async () => {
    await api.settings.set('tos', true);
    const fetchedData = await getData(tab);
    setState({ ...state, tos: true, ...fetchedData });
  };

  if (!state.tos) {
    return <TOSContainer onAccept={onAccept} />;
  }

  return (
    <PopupContainer
      tab={tab}
      currentDomain={currentDomain}
      interests={state.interests}
      stats={state.stats}
      isActive={state.isActive}
      showRelayFeature={state.showRelayFeature}
    />
  );
}

App.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  data: PropTypes.object.isRequired,
  tab: PropTypes.objectOf(PropTypes.string).isRequired,
  currentDomain: PropTypes.string,
};

App.defaultProps = {
  currentDomain: undefined,
};

api.tabs.getCurrent.then(async (tab) => {
  const settings = await api.settings.get();
  let initialData = {
    interests: [],
    stats: {
      totalTrackers: 0,
      page: {
        trackers: 0,
        cookies: 0,
      },
    },
    isActive: false,
  };

  if (settings.tos) {
    // If TOS were accepted, then fetch data from background script
    initialData = await getData(tab);
  }

  let currentDomain = null;
  if (tab && tab.url) {
    const currentURL = new URL(tab.url);
    if (currentURL.protocol === 'http:' || currentURL.protocol === 'https:') {
      currentDomain = currentURL.host.replace(/^www\./, '');
    }
  }

  render(<App
    data={{ tos: settings.tos, showRelayFeature: !settings.api_key, ...initialData }}
    tab={tab}
    currentDomain={currentDomain}
  />, document.getElementById('app'));
});
