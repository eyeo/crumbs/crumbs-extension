import { h, Fragment } from 'preact';
import PropTypes from 'prop-types';
import CrumbsLogo from '../assets/img/CrumbsFullLogo.svg';
import Intl from '../common/components/Intl';

export default function TOSContainer({ onAccept }) {
  return (
    <Fragment>
      <div className="flex flex-col space-y-6 text-sm">
        <span className="text-green">
          <CrumbsLogo style={{ height: 30 }} />
        </span>
        <div className="flex flex-col space-y-3 text-gray-5">
          <Intl name="popup_tos_text" nodeType="p" />
        </div>
        <div>
          <button
            type="button"
            onClick={onAccept}
            className="focus:outline-none rounded py-2 px-4 transition-colors duration-300
                       ease-in-out bg-green text-white hover:bg-opacity-75 w-full"
          >
            <Intl name="popup_tos_accept" />
          </button>
        </div>
      </div>
      <div
        className="bg-green fixed bottom-0 left-0 w-full"
        style={{
          height: 3,
        }}
      />
    </Fragment>
  );
}

TOSContainer.propTypes = {
  onAccept: PropTypes.func.isRequired,
};
