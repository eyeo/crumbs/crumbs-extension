import { h, Fragment, createRef } from 'preact';
import { useState } from 'preact/hooks';
import PropTypes from 'prop-types';
import ContentCard from '../components/common/ContentCard';
import CrumbsFeatureToggle from '../components/common/CrumbsFeatureToggle';
import CmpOptIn from '../components/CmpOptIn';
import Intl from '../../common/components/Intl';


export default function Settings(props) {
  const { flags, onChange } = props;
  const [showOptIn, setShowOptIn] = useState(false);
  const block3rdCookiesSelectRef = createRef();

  const onCmpClick = () => {
    if (!flags.block_cmps) {
      setShowOptIn(true);
    } else {
      onChange('block_cmps', !flags.block_cmps);
    }
  };

  const onOptInDismiss = () => {
    setShowOptIn(false);
  };

  const onOptInAccept = () => {
    onChange('block_cmps', !flags.block_cmps);
    setShowOptIn(false);
  };

  return (
    <Fragment>
      <ContentCard title={<Intl name="options_trackers_title" />}>
        <Intl name="options_trackers_description" nodeType="p" className="mt-3" />
        <CrumbsFeatureToggle
          id="anti_fingerprinting"
          checked={flags.anti_fingerprinting}
          onChange={() => onChange('anti_fingerprinting', !flags.anti_fingerprinting)}
          title={<Intl name="options_feature_anti_fingerprinting_title" />}
          description={<Intl name="options_feature_anti_fingerprinting_description" />}
        />
        <CrumbsFeatureToggle
          id="block_3rd_cookies"
          checked={!!flags.cookies_policy}
          onChange={(e) => {
            const { options, selectedIndex } = block3rdCookiesSelectRef.current;
            const value = e.target.checked ? parseInt(options[selectedIndex].value, 10) : 0;
            onChange('cookies_policy', value);
          }}
          title={<Intl name="options_feature_block_3rd_cookies_title" />}
          description={<Intl name="options_feature_block_3rd_cookies_description" nodeType="p" />}
        >
          <div className="mt-4">
            <Intl
              name="options_feature_block_3rd_cookies_protection_type"
              className="text-black mr-1"
            />
            <select
              ref={block3rdCookiesSelectRef}
              className={
              `form-select text-black ml-1 rounded-sm border-0 focus:ring-0
              text-sm leading-4 pt-1 pb-1 bg-magenta-light`
            }
              disabled={!flags.cookies_policy}
              value={flags.cookies_policy ? flags.cookies_policy : undefined}
              onChange={(e) => onChange('cookies_policy', parseInt(e.target.value, 10))}
            >
              <option value={1}>
                <Intl name="options_feature_block_3rd_cookies_expire" />
              </option>
              <option value={2}>
                <Intl name="options_feature_block_3rd_cookies_all" />
              </option>
            </select>
          </div>
        </CrumbsFeatureToggle>
        <CrumbsFeatureToggle
          id="hide_referrer"
          checked={flags.hide_referrer}
          onChange={() => onChange('hide_referrer', !flags.hide_referrer)}
          title={<Intl name="options_feature_hide_referrer_title" />}
          description={<Intl name="options_feature_hide_referrer_description" nodeType="p" />}
        />
        <CrumbsFeatureToggle
          id="remove_utm_params"
          checked={flags.remove_marketing_parameters}
          onChange={() => onChange('remove_marketing_parameters', !flags.remove_marketing_parameters)}
          title={<Intl name="options_feature_utm_params_title" />}
          description={<Intl name="options_feature_utm_params_description" nodeType="p" />}
        />
      </ContentCard>
      <ContentCard title={<Intl name="options_privacy_signals_title" />}>
        {/* <CrumbsFeatureToggle
          id="dnt"
          checked={flags.dnt}
          onChange={() => onChange('dnt', !flags.dnt)}
          title={<Intl name="options_feature_dnt_title" />}
          description={<Intl name="options_feature_dnt_description" nodeType="p" />}
        /> */}
        <CrumbsFeatureToggle
          id="gpc"
          checked={flags.gpc}
          onChange={() => onChange('gpc', !flags.gpc)}
          title={<Intl name="options_feature_gpc_title" />}
          description={<Intl name="options_feature_gpc_description" />}
        />
      </ContentCard>
      <ContentCard title={<Intl name="options_general_title" />}>
        <CrumbsFeatureToggle
          id="block_cmps"
          checked={flags.block_cmps}
          onChange={onCmpClick}
          title={<Intl name="options_feature_block_cmps_title" />}
          description={<Intl name="options_feature_block_cmps_description" nodeType="p" />}
        />
        {
          showOptIn && <CmpOptIn onAccept={onOptInAccept} onDismiss={onOptInDismiss} />
        }
        <CrumbsFeatureToggle
          id="share_statistics"
          checked={flags.share_statistics}
          onChange={() => onChange('share_statistics', !flags.share_statistics)}
          title={<Intl name="options_feature_anon_analytics_title" />}
          description={<Intl name="options_feature_anon_analytics_description" nodeType="p" />}
        />
      </ContentCard>
    </Fragment>
  );
}

Settings.propTypes = {
  flags: PropTypes.objectOf(
    PropTypes.oneOf([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
    ]),
  ).isRequired,
  onChange: PropTypes.func,
};

Settings.defaultProps = {
  onChange: () => {},
};
