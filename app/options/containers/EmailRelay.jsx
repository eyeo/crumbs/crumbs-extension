import { Fragment, h } from 'preact';
import PropTypes from 'prop-types';
import ContentCard from '../components/common/ContentCard';
import { CRUMBS_RELAY_URL } from '../../../config';
import Intl from '../../common/components/Intl';

function EmailRelay({ isAuthenticated, enabled, onChange }) {
  const onBtnClick = () => {
    if (!isAuthenticated) {
      window.location = `${CRUMBS_RELAY_URL}/account/signup/`;
      return;
    }

    if (!enabled) {
      onChange('relay', true);
      return;
    }

    window.location = `${CRUMBS_RELAY_URL}/account/profile/`;
  };

  const text = <Intl name={isAuthenticated && enabled ? 'options_relay_manage' : 'options_relay_activate'} />;

  return (
    <Fragment>
      <ContentCard title={<Intl name="options_relay_title" />} className="transition-height duration-1000 ease-in-out">
        <Intl name="options_relay_description" nodeType="p" className="mt-3" />
        <div className="flex justify-between items-center mt-6">
          <div>
            <button
              type="button"
              onClick={onBtnClick}
              className="text-white font-bold py-2 rounded-sm w-40 outline-none focus:outline-none bg-green"
            >
              {text}
            </button>
          </div>
          <div>
            {
              enabled && (
                <button
                  type="button"
                  className="text-red underline"
                  onClick={() => onChange('relay', false)}
                >
                  <Intl name="options_relay_disable" />
                </button>
              )
            }
          </div>
        </div>
        <Intl name="options_relay_watch" nodeType="p" className="font-bold mt-6" />
        <div className="mt-3 relative" style={{ paddingBottom: 'calc(var(--aspect-ratio, .5625) * 100%)' }}>
          <iframe
            className="absolute inset-0 h-full w-full"
            width="560"
            height="315"
            src="https://www.youtube-nocookie.com/embed/WGvGbOF7qw8?controls=0&modestbranding=1&rel=0"
            title="Email relay presentation video"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
      </ContentCard>
    </Fragment>
  );
}

EmailRelay.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  enabled: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default EmailRelay;
