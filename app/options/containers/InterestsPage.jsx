import { h, Fragment } from 'preact';
import PropTypes from 'prop-types';
import { useState } from 'preact/hooks';
import ContentCard from '../components/common/ContentCard';
import { InterestsContainer } from '../components/Insight';
import Intl from '../../common/components/Intl';


function InterestsPage({ interests, enable, disable }) {
  const enabledInterests = [];
  const disabledInterests = [];

  interests.forEach((insight) => {
    if (insight.enabled) {
      enabledInterests.push(insight);
    } else {
      disabledInterests.push(insight);
    }
  });

  const INTERESTS_BATCH_SIZE = 16;
  const [enabledSize, setEnabledSize] = useState(INTERESTS_BATCH_SIZE);
  const [disabledSize, setDisabledSize] = useState(INTERESTS_BATCH_SIZE);

  const topEnabledInterests = enabledInterests.splice(0, 15);

  return (
    <Fragment>
      <ContentCard
        title={<Intl name="options_interests_title" />}
        className="transition-height duration-1000 ease-in-out"
      >
        <Intl name="options_interests_description_short" nodeType="p" className="font-bold" />
        <Intl name="options_interests_description_long" nodeType="p" className="mt-3 mb-5" />
        {
          topEnabledInterests.length > 0 && (
            <div className="bg-gray-7 pl-5 pr-5 pb-5 -ml-5 -mr-5 border-l-4 border-green rounded">
              <Intl
                name="options_interests_relevant_title"
                nodeType="h2"
                className="text-base font-bold text-green pt-3"
              />
              <InterestsContainer
                interests={topEnabledInterests}
                onInterestAction={disable}
              />
            </div>
          )
        }

        <InterestsContainer
          interests={enabledInterests}
          onInterestAction={disable}
          visibleSize={enabledSize}
          onShowMore={() => setEnabledSize(enabledSize + INTERESTS_BATCH_SIZE)}
        />

        <div className="flex justify-between mt-10 mb-1">
          <h2 className="text-base font-bold text-black">
            <Intl name="options_interests_disabled_title" params={[disabledInterests.length]} />
          </h2>
        </div>
        <Intl name="options_interests_disabled_description" nodeType="p" />
        <InterestsContainer
          interests={disabledInterests}
          onInterestAction={enable}
          visibleSize={disabledSize}
          onShowMore={() => setDisabledSize(disabledSize + INTERESTS_BATCH_SIZE)}
        />
      </ContentCard>
    </Fragment>
  );
}

InterestsPage.propTypes = {
  interests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      emoji: PropTypes.string,
      enabled: PropTypes.bool,
    }),
  ),
  enable: PropTypes.func.isRequired,
  disable: PropTypes.func.isRequired,
};

InterestsPage.defaultProps = {
  interests: [],
};

export default InterestsPage;
