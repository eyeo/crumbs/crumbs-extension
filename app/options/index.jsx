import { h, render } from 'preact';
import { useState } from 'preact/hooks';
import PropTypes from 'prop-types';
import * as Sentry from '@sentry/react';

import api from '../common/api';
import Settings from './containers/Settings';
import InterestsPage from './containers/InterestsPage';
import EmailRelay from './containers/EmailRelay';
import Menu from './components/Menu';
import Intl from '../common/components/Intl';
import '../assets/css/main.css';
import { parseInsights } from '../common/insights-parser';


Sentry.init({
  dsn: process.env.SENTRY_DSN,
  tunnel: 'https://raven.crumbs.org',
  release: browser.runtime.getManifest().version,
  environment: process.env.ENV_NAME,
  tracesSampleRate: 0.1,
});

const extractCurrentLocation = () => {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get('path');
};

const App = (props) => {
  const { insights, settings } = props;
  const [currentSettings, setCurrentSettings] = useState(settings);
  const [currentInsights, setCurrentInsights] = useState(insights);

  const onChange = async (name, value) => {
    const result = await api.settings.set(name, value);
    if (result) {
      setCurrentSettings(result);
    }
  };

  const refreshInsights = async () => {
    const freshInsights = await api.insights.all();
    setCurrentInsights(parseInsights(freshInsights));
  };

  const enableInsight = async (id) => {
    await api.insights.enable(id);
    await refreshInsights();
  };

  const disableInsight = async (id) => {
    await api.insights.disable(id);
    await refreshInsights();
  };

  const renderPage = (currentPage) => {
    if (currentPage === 'insights') {
      return (
        <InterestsPage
          interests={currentInsights}
          enable={enableInsight}
          disable={disableInsight}
        />
      );
    }

    if (currentPage === 'relay') {
      return (
        <EmailRelay
          isAuthenticated={!!currentSettings.api_key}
          enabled={currentSettings.relay}
          onChange={onChange}
        />
      );
    }

    return <Settings flags={currentSettings} onChange={onChange} />;
  };

  const [currentPage, setPageChange] = useState(extractCurrentLocation());
  const onPageChange = (path) => {
    window.history.replaceState(0, document.title, `${window.location.pathname}?path=${path}`);
    setPageChange(path);
    window.scrollTo(0, 0);
  };

  return (
    <div className="bg-gray-6 p-24 mx-auto relative" style={{ maxWidth: 1300 }}>
      <div className="fixed top-0 pt-24 h-full min-h-screen">
        <Menu
          className="mt-8"
          currentLocation={currentPage}
          onPageChange={onPageChange}
          showRelayFeature={!settings.api_key}
        />
      </div>
      <div className="ml-64">
        {renderPage(currentPage)}

        <div className="flex justify-between max-w-screen-md text-gray-4 px-5">
          <span>&copy; { new Date().getFullYear() } Crumbs. <Intl name="options_all_rights_reserved" /></span>
          <a
            className="cursor-pointer underline hover:no-underline"
            href="https://crumbs.org/privacy/"
            target="_blank"
            rel="noopener noreferrer"
            title="Crumbs Privacy Policy"
          >
            <Intl name="options_privacy_policy" />
          </a>
        </div>
      </div>
    </div>
  );
};

App.propTypes = {
  insights: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      emoji: PropTypes.string,
      enabled: PropTypes.bool,
    }),
  ),
  settings: PropTypes.objectOf(
    PropTypes.oneOf([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
    ]),
  ).isRequired,
};

App.defaultProps = {
  insights: undefined,
};

Promise.all([api.insights.all(), api.settings.get()]).then(([insights, settings]) => {
  render(<App insights={parseInsights(insights)} settings={settings} />, document.getElementById('app'));
});
