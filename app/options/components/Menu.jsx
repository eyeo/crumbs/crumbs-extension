import { h } from 'preact';
import PropTypes from 'prop-types';
import CrumbsLogo from '../../assets/img/CrumbsFullLogo.svg';
import GearIcon from '../../assets/img/GearIcon.svg';
import InsightsIcon from '../../assets/img/InsightsIcon.svg';
import EmailRelayIcon from '../../assets/img/EmailRelayIcon.svg';
import MenuButton from './common/MenuButton';
import Intl from '../../common/components/Intl';
import { CRUMBS_WEBSITE_URL } from '../../../config';


export default function Menu(props) {
  const {
    className, currentLocation, onPageChange, showRelayFeature,
  } = props;

  return (
    <div className={`${className}`}>
      <div className="mb-2 text-green">
        <a href="https://crumbs.org" target="_blank" rel="noopener noreferrer" aria-label="Crumbs">
          <CrumbsLogo style={{ height: 45 }} />
        </a>
        <div className="text-center text-gray-4 mt-2">
          v{ process.env.ADDON_VERSION }
        </div>
      </div>
      <div className="text-white flex flex-col py-8">
        <div className="my-4">
          <MenuButton
            name={<Intl name="options_menu_settings" />}
            className={`${currentLocation === 'settings' ? 'opacity-100' : 'opacity-50'}
            hover:opacity-100 transition-opacity duration-300 ease-in-out`}
            onClick={() => onPageChange('settings')}
          >
            <GearIcon className="fill-current" />
          </MenuButton>
        </div>
        <div className="my-4">
          <MenuButton
            name={<Intl name="options_menu_interests" />}
            className={`${currentLocation === 'insights' ? 'opacity-100' : 'opacity-50'}
            hover:opacity-100 transition-opacity duration-300 ease-in-out`}
            background="bg-green"
            onClick={() => onPageChange('insights')}
          >
            <InsightsIcon className="fill-current" />
          </MenuButton>
        </div>
        <div className="my-4 flex items-center relative">
          <MenuButton
            name={<Intl name="options_menu_email_relay" />}
            className={`${currentLocation === 'relay' ? 'opacity-100' : 'opacity-50'}
            hover:opacity-100 transition-opacity duration-300 ease-in-out`}
            background="bg-yellow"
            onClick={() => onPageChange('relay')}
          >
            <EmailRelayIcon className="fill-current" />
          </MenuButton>
          {
            showRelayFeature && (
              <span className="flex h-3 w-3 absolute" style={{ top: -10, left: 37 }}>
                <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-yellow opacity-75" />
                <span className="relative inline-flex rounded-full h-full w-full bg-yellow" />
              </span>
            )
          }
        </div>
        <div className="my-12" style={{ maxWidth: 220 }}>
          <Intl
            name="popup_found_issue"
            nodeType="a"
            href={`${CRUMBS_WEBSITE_URL}/report-issue/?version=${process.env.ADDON_VERSION}`}
            className="text-gray-4 underline hover:no-underline focus:outline-none"
            target="_blank"
            rel="noopener noreferrer"
          />
        </div>
      </div>
    </div>
  );
}

Menu.propTypes = {
  className: PropTypes.string,
  currentLocation: PropTypes.string,
  onPageChange: PropTypes.func,
  showRelayFeature: PropTypes.bool,
};

Menu.defaultProps = {
  className: '',
  currentLocation: 'settings',
  onPageChange: () => {},
  showRelayFeature: false,
};
