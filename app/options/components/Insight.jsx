/* eslint-disable react/jsx-props-no-spreading */
import { h, Fragment } from 'preact';
import { useState } from 'preact/hooks';
import Tippy from '@tippyjs/react/headless';
import PropTypes from 'prop-types';
import DotsHorizontalIcon from '../../assets/img/DotsHorizontalIcon.svg';
import Intl from '../../common/components/Intl';


function Interest({
  interestId, title, icon, enabled, onClick,
}) {
  const [visible, setVisible] = useState(false);
  const show = () => setVisible(true);
  const hide = () => setVisible(false);

  return (
    <div className="w-1/2 p-2">
      <div className="flex justify-between bg-gray-1 bg-opacity-75 rounded px-3 py-2">
        <div className="flex items-center w-11/12">
          <span className="mr-2 text-xl leading-6">{icon}</span>
          <span className="text-black truncate">{title}</span>
        </div>
        <Tippy
          placement="bottom-end"
          interactive
          visible={visible}
          offset={[0, 2]}
          onClickOutside={hide}
          render={(attrs) => (
            <div
              style={{ width: 232 }}
              tabIndex="-1"
              className="p-4 z-50 bg-white flex flex-col space-y-2 text-xs rounded-sm focus:outline-none shadow-md"
              {...attrs}
            >
              <h4 className="font-bold text-black">{title}</h4>
              <Intl name="options_interests_menu_description" nodeType="p" className="text-gray-5" />
              <div>
                <button
                  onClick={() => {
                    onClick(interestId);
                    hide();
                  }}
                  type="button"
                  className="float-right text-green font-medium focus:outline-none"
                >
                  <Intl name={enabled ? 'options_interests_menu_disable' : 'options_interests_menu_enable'} />
                </button>
              </div>
            </div>
          )}
        >
          <button
            type="button"
            className="focus:outline-none"
            onClick={visible ? hide : show}
          >
            <DotsHorizontalIcon
              className="opacity-75 cursor-pointer"
              style={{ width: 14 }}
            />
          </button>
        </Tippy>
      </div>
    </div>
  );
}

Interest.propTypes = {
  interestId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  enabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};


function InterestsContainer({
  interests, style, visibleSize,
  onInterestAction, onShowMore,
}) {
  let ShowMoreButton;
  if (visibleSize > 0 && visibleSize < interests.length) {
    ShowMoreButton = () => (
      <button
        type="button"
        onClick={onShowMore}
        className={`
          font-bold py-2 rounded-sm w-40 text-green border border-green block mx-auto mt-6
          outline-none focus:outline-none transition-colors ease-in-out duration-300
          hover:bg-green hover:bg-opacity-25
        `}
      >
        <Intl name="options_interests_show_more" />
      </button>
    );
  }

  return (
    <Fragment>
      <div className="flex flex-wrap -mx-2 mt-6" style={style}>
        {
          (visibleSize > 0 ? interests.splice(0, visibleSize) : interests).map((item) => (
            <Interest
              interestId={item.id}
              title={item.name}
              icon={item.emoji}
              enabled={item.enabled}
              onClick={onInterestAction}
            />
          ))
        }
      </div>
      <ShowMoreButton />
    </Fragment>
  );
}

InterestsContainer.propTypes = {
  onInterestAction: PropTypes.func.isRequired,
  onShowMore: PropTypes.func,
  style: PropTypes.objectOf(PropTypes.string),
  interests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      emoji: PropTypes.string,
      enabled: PropTypes.bool,
    }),
  ).isRequired,
  visibleSize: PropTypes.number,
};

InterestsContainer.defaultProps = {
  style: null,
  visibleSize: -1,
  onShowMore: null,
};

export {
  Interest,
  InterestsContainer,
};
