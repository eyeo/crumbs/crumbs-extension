import { h } from 'preact';
import PropTypes from 'prop-types';
import Modal from './common/Modal';
import Intl from '../../common/components/Intl';

export default function CmpOptIn(props) {
  const { onAccept, onDismiss } = props;

  return (
    <Modal>
      <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
        <div className="sm:flex sm:items-start">
          <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
            <Intl
              name="options_feature_block_cmps_modal_title"
              nodeType="h3"
              className="text-lg leading-6 font-bold text-black"
            />
            <div className="mt-4">
              <Intl
                name="options_feature_block_cmps_modal_body"
                nodeType="p"
                className="text-sm leading-5 text-gray-5"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="bg-gray-7 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
        <span className="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
          <button
            type="button"
            onClick={onAccept}
            className="inline-flex justify-center w-full rounded-md
              border border-transparent px-4 py-2 bg-green text-base
              leading-6 font-bold text-white shadow-sm hover:opacity-75
              focus:outline-none transition ease-in-out duration-150
              sm:text-sm sm:leading-5"
          >
            <Intl name="options_feature_block_cmps_modal_accept_button" />
          </button>
        </span>
        <span className="mt-3 flex w-full rounded-md sm:mt-0 sm:w-auto">
          <button
            type="button"
            onClick={onDismiss}
            className="inline-flex justify-center w-full rounded-md
              px-4 py-2 text-gray-5 text-base leading-6 font-bold
              text-white focus:outline-none hover:opacity-75
              transition ease-in-out duration-150 sm:text-sm sm:leading-5"
          >
            <Intl name="options_feature_block_cmps_modal_cancel_button" />
          </button>
        </span>
      </div>
    </Modal>
  );
}

CmpOptIn.propTypes = {
  onAccept: PropTypes.func,
  onDismiss: PropTypes.func,
};

CmpOptIn.defaultProps = {
  onAccept: () => {},
  onDismiss: () => {},
};
