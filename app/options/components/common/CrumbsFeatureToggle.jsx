import { h } from 'preact';
import PropTypes from 'prop-types';

function CrumbsFeatureToggle(props) {
  const {
    id, checked, disabled, onChange,
    title, description,
    children,
  } = props;

  return (
    <form className={`mt-6 ${disabled ? 'opacity-75' : ''}`}>
      <div className="inline-flex items-center cursor-pointer mb-2">
        <input
          id={id}
          type="checkbox"
          disabled={disabled}
          className={
            `form-checkbox border-gray-2 rounded-xs border-2 w-5 h-5 text-green
            focus:ring-0 focus:ring-offset-0 ${disabled ? 'cursor-not-allowed' : 'cursor-pointer'}`
          }
          checked={checked}
          onChange={onChange}
        />
        <label
          className={`text-base text-black font-bold ml-3 ${disabled ? 'cursor-not-allowed' : 'cursor-pointer'}`}
          htmlFor={id}
        >
          { title }
        </label>
      </div>
      <div style={{ marginLeft: '1.875rem' }}>
        <p className="text-gray-5">{ description }</p>
        { children }
      </div>
    </form>
  );
}

CrumbsFeatureToggle.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

CrumbsFeatureToggle.defaultProps = {
  checked: false,
  disabled: false,
  onChange: () => {},
  children: null,
};

export default CrumbsFeatureToggle;
