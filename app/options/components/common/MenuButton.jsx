import { h } from 'preact';
import PropTypes from 'prop-types';

export default function MenuButton(props) {
  const {
    className, name, onClick, children, background,
  } = props;

  const wrappedOnClick = (event) => {
    event.stopPropagation();
    onClick();
  };

  return (
    <button
      type="button"
      className={`${className} flex items-center focus:outline-none`}
      onClick={wrappedOnClick}
    >
      <div
        style={{ height: 40, width: 40 }}
        className={`${background || 'bg-red'} mr-4 rounded-sm flex items-center justify-center`}
      >
        {children}
      </div>
      <span className="text-black text-xl">{name}</span>
    </button>
  );
}

MenuButton.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  background: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.func.isRequired,
};

MenuButton.defaultProps = {
  className: '',
  onClick: () => {},
  background: 'bg-red',
};
