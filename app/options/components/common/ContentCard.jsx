import { h } from 'preact';
import PropTypes from 'prop-types';

function ContentCard(props) {
  const { title, children, className } = props;

  return (
    <div className={`${className} max-w-screen-md rounded-lg shadow-lg bg-white py-10 px-12 mb-8`}>
      <div className="text-black text-2xl font-bold mb-6">{ title }</div>
      { children }
    </div>
  );
}

ContentCard.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

ContentCard.defaultProps = {
  className: '',
  children: null,
};

export default ContentCard;
