/*
 * This file is part of Crumbs <https://crumbs.org/>,
 * Copyright (C) 2020-present eyeo GmbH
 */

module.exports = function (api) {
  const presets = [
    '@babel/preset-env',
  ];
  const plugins = [
    ['@babel/plugin-transform-runtime'], // To reuse babel helpers in order to reduce code size
  ];

  switch (api.env()) {
    case 'app':
      plugins.push(['@babel/plugin-transform-react-jsx', { pragma: 'h', pragmaFrag: 'Fragment' }]);
      break;
    case 'src':
      break;
    default:
      break;
  }

  return {
    presets,
    plugins,
  };
};
