
module.exports = {
  // Global options:
  verbose: true,
  // Command options:
  artifactsDir: 'artifacts',
  sourceDir: 'dist',
  build: {
    overwriteDest: true,
  },
  run: {
    // Open a page with the crumbs.org
    startUrl: ['https://crumbs.org'],
  },
};
