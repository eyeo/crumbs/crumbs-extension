#!/bin/bash

for file in artifacts/*.zip; do
    # Create Firefox build file
    cp "${file}" "${file/%.zip/.xpi}"
    # Create Opera build file
    cp "${file}" "${file/%.zip/.crx}"
done