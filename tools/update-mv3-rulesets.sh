#!/bin/bash

echo "🛠  Building subscriptions and rulesets. This might take a while..."

# Copy custom subscriptions in the @eyeo/webext-ad-filtering-solution packages
mkdir -p node_modules/@eyeo/webext-ad-filtering-solution/scriptsOutput/
cp packages/adblocker/custom-subscriptions.json node_modules/@eyeo/webext-ad-filtering-solution/scriptsOutput/

# Fetch and convert rules to MV3 format
npm run --prefix node_modules/@eyeo/webext-ad-filtering-solution subs-fetch &&
npm run --prefix node_modules/@eyeo/webext-ad-filtering-solution subs-convert
# npm run --prefix node_modules/@eyeo/webext-ad-filtering-solution subs-generate

# Copy generated MV3 rules
# cp node_modules/@eyeo/webext-ad-filtering-solution/scriptsOutput/rulesets/* rulesets/

# Copy updated custom-subscriptions.json
cp node_modules/@eyeo/webext-ad-filtering-solution/scriptsOutput/custom-subscriptions.json packages/adblocker/custom-subscriptions.json

echo "✅ Rulesets and subscriptions generated."
