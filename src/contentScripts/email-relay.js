import browser from 'webextension-polyfill';
import settings from '../lib/settings';

const MENU_WIDTH = 300;
const MENU_HEIGHT = 143;
const ICON_WIDTH = 16;
const ICON_HEIGHT = 16;
const ICON_PADDING = 5;
const ICON_IMG_URL = 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAA'
  + 'AsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFESURBVHgBrVXBTcNAEByHBlyCO4A/Qjo6cAfkw4MXoQKHCuCH5Dzs'
  + 'DqxUcEgh4pkSDBUYCiDHLT47G7M+O0pGGlu62529Xe+tgX4oy8yytDSOleXG8tkywkiojoiP2ZBwMlKIk4JfnEqMlyLiYtMB48Klpz12Gy5Y9gj'
  + 'NhEwiy1ywL4ZOF7cSOgvx/hJ1hOcscMw3Cshfr8Y6vccqrfCWGvsusV4kzPdf+wQuQtiJThGXWC0UAqPRhfm5xtXdKwRMBDHCp/OMIXqd3aAHk5'
  + '71r1rPhOLuFt/wCH4I6+d/z2Cby16BxoGCdapUJ2Me21VDJzcPuLxduhXxTs8gt41qLZq2ofcOCXZXb6+mZFRBbmyp+GT/BLnVQp6i7/Lzq1d57P'
  + 'YwxwmHwzGidDJxfDWYYvyA1ThgclNdc9RjiafW/AKU5PQLXqva3UUaKU0AAAAASUVORK5CYII=")';
const DEFAULT_INPUT_SELECTOR = 'input:not([disabled])[type="email"],input:not([disabled])[type="text"]';


class CrumbsEmailMenu {
  constructor(selector = DEFAULT_INPUT_SELECTOR) {
    this.selector = selector;
    this.activeInput = null;
    this.menuContainer = null;
    this.inputs = new Set();

    // Create a MutationObserver to watch for dynamically generated email inputs
    this.mutationObserver = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.addedNodes.length > 0) {
          const inputs = mutation.target.querySelectorAll(this.selector);
          for (const input of inputs) {
            this.setIcon(input);
          }
        }
      });
    });
  }

  static isIconCoordonates(inputElement, x, y) {
    const rect = inputElement.getBoundingClientRect();
    const { bottom, top, right } = rect;

    return x >= right - ICON_WIDTH - ICON_PADDING
      && x <= right - ICON_PADDING
      && y >= top
      && y <= bottom;
  }

  onDocumentClick(event) {
    // clicks on inputs with crumbs symbol are treated by onInputClick method
    for (const input of this.inputs) {
      if (input.contains(event.target)) {
        return;
      }
    }

    // if menu is opened and a click occurred outside of the menu
    if (this.menuContainer && !this.menuContainer.contains(event.target)) {
      this.close();
    }
  }

  onInputClick(event) {
    if (CrumbsEmailMenu.isIconCoordonates(event.target, event.clientX, event.clientY)) {
      this.close();
      this.open(event.target);
    }
  }

  static onInputMouseMove(event) {
    const element = event.target;
    if (CrumbsEmailMenu.isIconCoordonates(event.target, event.clientX, event.clientY)) {
      element.style.cursor = 'pointer';
    } else {
      element.style.cursor = 'auto';
    }
  }

  close() {
    if (this.menuContainer) {
      this.menuContainer.remove();
      this.menuContainer = null;
      this.activeInput = null;
    }
  }

  open(inputElement) {
    this.activeInput = inputElement;
    const r = inputElement.getBoundingClientRect();
    const rightCornerX = Math.max(window.pageXOffset + r.right, MENU_WIDTH);
    const rightCornerY = window.pageYOffset + r.bottom - (r.height / 2) + (ICON_HEIGHT / 2) + ICON_PADDING;
    // create container for the menu iframe
    this.menuContainer = document.createElement('div');
    this.menuContainer.style = `
    position: absolute; width: ${MENU_WIDTH}px; height: ${MENU_HEIGHT}px; top: ${rightCornerY}px;
    left: ${rightCornerX - MENU_WIDTH}px; z-index: 2147483647; border-radius: 10px;
    box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.3);
  `;
    // include menu iframe from the extension
    const iframe = document.createElement('iframe');
    iframe.style = 'width: 100%; height: 100%; border: 0px; margin: 0px auto;';
    iframe.src = `${browser.runtime.getURL('crumbs-email-relay-menu.html')}?domain=${window.location.host}`;
    this.menuContainer.appendChild(iframe);

    document.body.appendChild(this.menuContainer);
  }

  setIcon(inputElement) {
    if (!CrumbsEmailMenu.shouldAddIcon(inputElement)) {
      return;
    }

    if (this.inputs.has(inputElement)) {
      return;
    }

    this.inputs.add(inputElement);
    const inlineStyles = inputElement.style;
    inlineStyles.backgroundImage = ICON_IMG_URL;
    inlineStyles.backgroundRepeat = 'no-repeat';
    inlineStyles.backgroundAttachment = 'scroll';
    inlineStyles.backgroundSize = `${ICON_WIDTH}px ${ICON_HEIGHT}px`;
    inlineStyles.backgroundPosition = `right ${ICON_PADDING}px center`;

    inputElement.removeEventListener('click', this.onInputClick.bind(this));
    inputElement.addEventListener('click', this.onInputClick.bind(this));
    inputElement.removeEventListener('mousemove', CrumbsEmailMenu.onInputMouseMove);
    inputElement.addEventListener('mousemove', CrumbsEmailMenu.onInputMouseMove);
  }

  static shouldAddIcon(input) {
    const lookupAttributes = [
      'type', 'name', 'placeholder', 'id', 'title', 'aria-label',
      'data-tooltip', 'autocomplete',
    ];

    for (const attrName of lookupAttributes) {
      const value = (input.getAttribute(attrName) || '').toLowerCase();
      if (value.indexOf('email') > -1 || value.indexOf('e-mail') > -1) {
        return true;
      }
    }

    return false;
  }

  addMenuIcon() {
    const inputs = document.querySelectorAll(this.selector);
    for (const input of inputs) {
      this.setIcon(input);
    }

    document.removeEventListener('click', this.onDocumentClick.bind(this));
    document.addEventListener('click', this.onDocumentClick.bind(this));
  }

  observeDOMChanges() {
    this.mutationObserver.observe(document.body, { childList: true, subtree: true });
  }

  fillActiveInput(email) {
    if (this.activeInput) {
      this.activeInput.value = email;
      this.fireOnChange();
    }
  }

  fireOnChange() {
    if (this.activeInput) {
      const event = new Event('change', { bubbles: true, cancelable: true });
      this.activeInput.dispatchEvent(event);
    }
  }
}

settings.getAll().then(({ tos, relay }) => {
  // user didn't accept TOS
  if (!tos) {
    return;
  }

  // user is logged in and has disabled relay manually
  if (!relay) {
    return;
  }

  const menu = new CrumbsEmailMenu();
  // Catch all immediately findable email inputs
  menu.addMenuIcon();

  let monitoringStarted = false;
  if (document.body) {
    menu.observeDOMChanges();
    monitoringStarted = true;
  }

  // Catch email inputs that only become findable after
  // the entire page (including JS/CSS/images/etc) is fully loaded.
  window.addEventListener('load', () => {
    menu.addMenuIcon();
    if (!monitoringStarted) {
      menu.observeDOMChanges();
    }
  });

  window.addEventListener('message', (message) => {
    if (message.data?.type === 'crumbs.menu.close') {
      menu.close();
    }

    if (message.data?.type === 'crumbs.menu.fill') {
      menu.fillActiveInput(message.data.email);
      menu.close();
    }
  }, false);
});

function addContextMenuListener() {
  let targetElement = null;

  document.addEventListener('contextmenu', (event) => {
    targetElement = event.target;
  }, true);

  browser.runtime.onMessage.addListener((msg) => {
    // This content script is added in all frames except crumbs.org. Meaning that on a specific tab it could be
    // injected more than once. In that case, this listener will triggerred multiple times (for each frame), but
    // `targetElement` will exist only on the frame where the user right-clicked to show the context menu.
    // If `targetElement` is null, do nothing, the next frame will handle the event.
    if (!targetElement) {
      return;
    }

    if (msg.type === 'contextmenu.fill_input') {
      targetElement.value = msg.value;
      const event = new Event('change', { bubbles: true, cancelable: true });
      targetElement.dispatchEvent(event);
    } else if (msg.type === 'contextmenu.copy') {
      const textArea = document.createElement('textarea');
      textArea.style.position = 'fixed';
      textArea.style.top = 0;
      textArea.style.left = 0;
      textArea.style.background = 'transparent';
      textArea.value = msg.value;

      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();

      document.execCommand('copy');
      document.body.removeChild(textArea);
    }
  });
}

addContextMenuListener();
