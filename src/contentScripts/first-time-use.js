import browser from 'webextension-polyfill';

const updateLocalSettings = (settings) => {
  if (settings) {
    const { gpc, block_cmps: cmp, tos } = settings;
    sessionStorage.setItem('crumbs-settings', JSON.stringify({
      gpc,
      block_cmps: cmp,
      tos,
    }));
  }
};

const updateExtensionSetting = (name, value) => {
  browser.runtime.sendMessage(
    {
      type: 'settings.set',
      name,
      value,
    },
  ).then(updateLocalSettings);
};

const retrieveSettingsFromExtension = () => {
  try {
    browser.runtime.sendMessage({ type: 'settings.get' }).then(updateLocalSettings);
  } catch (error) {
    console.error('Failed to retrieve extension settings', error);
    setTimeout(retrieveSettingsFromExtension, 50);
  }
};


retrieveSettingsFromExtension();

document.addEventListener('DOMContentLoaded', async () => {
  document.addEventListener('settings.change', (event) => {
    const { detail: { name, value } } = event;
    updateExtensionSetting(name, value);
  });
});
