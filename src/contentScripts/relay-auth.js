import browser from 'webextension-polyfill';

const { CRUMBS_RELAY_URL } = require('../../config');

function getToken() {
  const crumbsTokenElement = document.querySelector('crumbs-token');
  if (crumbsTokenElement) {
    const token = crumbsTokenElement.dataset.apiKey;
    browser.runtime.sendMessage({
      type: 'settings.set',
      name: 'api_key',
      value: token,
    });
    return token;
  }

  return null;
}

function sendSyncSignal() {
  browser.runtime.sendMessage({
    type: 'relay.sync',
  });
}

function syncAliases() {
  if (window.location.href.indexOf('account/profile') > 0) {
    sendSyncSignal();
  }
}

if (document.readyState === 'complete' || document.readyState === 'interactive') {
  getToken();
  syncAliases();
} else {
  window.addEventListener('load', () => {
    getToken();
    syncAliases();
  });
}

window.addEventListener('crumbs-alias-deleted', () => {
  sendSyncSignal();
});

/**
 * Relay website will send a message when it's ready to receive post messages from the extension.
 * We wait for this message before sending the associated aliases to the page.
 */
window.addEventListener('message', (event) => {
  if (event.source === window && event.data && event.data.message === 'crumbs-relay-ready') {
    browser.runtime.sendMessage({
      type: 'relay.getAliasToDomainMap',
    }).then((data) => {
      window.postMessage({
        message: 'crumbs-extension-associations',
        associations: data,
      }, CRUMBS_RELAY_URL);
    });
  }
});
