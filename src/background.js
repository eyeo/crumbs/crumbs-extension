import browser from 'webextension-polyfill';
import AdBlock from 'adblocker';
import AllowList from 'adblocker/src/allowlist';
import subscriptions from 'adblocker/custom-subscriptions.json';
import PatSDK from '@eyeo/crumbs-ext-sdk';
import * as Sentry from '@sentry/browser';
import settings from './lib/settings';
import stats from './lib/stats';
import port from './lib/port';
import {
  DEBUG,
  CRUMBS_RELAY_API_URL,
  CRUMBS_WEBSITE_URL,
  TELEMETRY_DSN,
  TELEMETRY_LOG_LEVEL,
} from '../config';
import toggleMenu from './lib/relay/relay-context-menu';
import Icons from './lib/icons';


export default class Background {
  static async init() {
    Sentry.init({
      dsn: process.env.SENTRY_DSN,
      tunnel: 'https://raven.crumbs.org',
      release: browser.runtime.getManifest().version,
      environment: process.env.ENV_NAME,
      tracesSampleRate: 0.1,
    });

    const manifest = browser.runtime.getManifest();

    // initialize AdBlock SDK
    await AdBlock.init({
      name: manifest.short_name,
      version: manifest.version,
      bundledSubscriptions: subscriptions,
      bundledSubscriptionsPath: 'subscriptions',
    });

    await AdBlock.toggleCMPSubscription(await settings.get('block_cmps'));

    // initialize PatSDK
    const config = await settings.getAll();
    await PatSDK.init({
      debug: DEBUG,
      app: {
        name: manifest.short_name,
        version: process.env.ADDON_VERSION, // manifest.version,
      },
      reportingConfig: {
        dsn: TELEMETRY_DSN,
        apiKey: process.env.TELEMETRY_AUTHORIZATION_TOKEN,
        logLevel: TELEMETRY_LOG_LEVEL,
      },
      isPageAllowlisted: async ({ tabId, pageURL }) => (AllowList.has(pageURL, tabId)),

      // feature flags
      reporting: config.share_statistics,
      gpc: config.gpc,
      urlParamsFilter: config.remove_marketing_parameters,
      hideReferrer: config.hide_referrer,
      fpShield: config.anti_fingerprinting,
      cookiePolicy: config.cookies_policy,
      proxy: false,
      interests: {
        coldStart: true,
      },
    });

    await stats.init();

    // add blocked request listener and increment the stats shown in the Popup UI
    AdBlock.onBlocked(({ request }) => {
      const { tabId } = request;
      stats.incrTrackersBlocked(tabId);
    });

    // add cookies blocked listener and increment the stats shown in the Popup UI
    PatSDK.cookies.onBlocked.addListener((detailsPerTab) => {
      for (const { tabId, cookiesBlocked } of detailsPerTab) {
        stats.incrCookiesExpired(tabId, cookiesBlocked).then(() => {});
      }
    });

    // TODO: remove this line after removing proxy feature was done
    await settings.remove('proxy'); // remove proxy feature flag
  }
}


(async () => {
  const tos = await settings.get('tos');
  if (tos) {
    Background.init();
  }
})();


port.on('settings.get', async () => (settings.getAll()));
port.on('settings.set', async (message) => {
  try {
    if (message.name === 'tos' && message.value && !await settings.get('tos')) {
      await Background.init();
    }

    if (message.name === 'api_key') {
      await settings.set('relay', true);
    }

    if (message.name === 'relay') {
      toggleMenu(message.value);
    }

    // do save
    await settings.set(message.name, message.value);

    if (message.name === 'cookies_policy') {
      await PatSDK.configureFeature('cookiePolicy', message.value);
    }

    if (message.name === 'block_cmps') {
      await AdBlock.toggleCMPSubscription(await settings.get('block_cmps'));
    }

    if (message.name === 'remove_marketing_parameters') {
      await PatSDK.configureFeature('urlParamsFilter', message.value);
    }

    if (message.name === 'hide_referrer') {
      await PatSDK.configureFeature('hideReferrer', message.value);
    }

    if (message.name === 'anti_fingerprinting') {
      await PatSDK.configureFeature('fpShield', message.value);
    }

    if (message.name === 'gpc') {
      await PatSDK.configureFeature('gpc', message.value);
    }

    // // !IMPORTANT: we call Telemetry.setState after we saved the new value for the setting,
    // // because we use settings.shareStatistics inside our telemetry logic.
    if (message.name === 'share_statistics') {
      await PatSDK.configureFeature('reporting', message.value);
    }
  } catch (e) {
    console.error(e);
    return false;
  }

  return settings.getAll();
});


port.on('filters.isAllowed', async (message) => AllowList.has(message.tab.url, message.tab.id));

port.on('filters.block', async (message) => {
  await AllowList.remove(message.tab.url);
  Icons.setEnabledIcons(message.tab.id);
});

port.on('filters.allow', async ({ tab }) => {
  await AllowList.add(tab.url);
  Icons.setDisabledIcons(tab.id);
  // TODO: send event using SDK
  // Telemetry.sendEvent('crumbs_paused');
});

port.on('popup.upsell', () => {
  // TODO: send event using SDK
  // Telemetry.sendEvent('popup_upsell', { feature });
});


port.on('crumbs.getInsights', async () => PatSDK.interests.getProfile());
port.on('crumbs.disableInsight', async ({ insightId }) => {
  const disableFunctions = {
    location: PatSDK.interests.disableLocation,
    language: PatSDK.interests.disableLanguage,
    default: PatSDK.interests.disable,
  };

  const disableFn = disableFunctions[insightId] || disableFunctions.default;
  return disableFn(insightId);
});
port.on('crumbs.enableInsight', async ({ insightId }) => {
  const enableFunctions = {
    location: PatSDK.interests.enableLocation,
    language: PatSDK.interests.enableLanguage,
    default: PatSDK.interests.enable,
  };

  const enableFn = enableFunctions[insightId] || enableFunctions.default;
  return enableFn(insightId);
});


/**
 * Listens for the email relay authentication request, extracts the X-Crumb-Api-Key header and
 * saves it for later use.
 */
browser.webRequest.onHeadersReceived.addListener(async (details) => {
  const { responseHeaders } = details;
  const apiKeyHeader = responseHeaders.find((header) => header.name.toLowerCase() === 'x-crumbs-api-key');
  const apiKey = apiKeyHeader?.value;

  if (apiKey) {
    await settings.set('api_key', apiKey);
  }
}, { urls: [`${CRUMBS_RELAY_API_URL}/v1.0/me/`] }, ['responseHeaders']);


/**
 * Insert custom stylesheet
 */
browser.webNavigation.onCommitted.addListener((details) => {
  const { tabId, frameId } = details;

  // inset CSS rules to be used by relay website in order to hide upsell banner
  browser.scripting.insertCSS({
    target: { tabId, frameIds: [frameId] },
    css: '.hide-if-crumbs-installed { display: none !important; }',
  }).catch((e) => {
    // eslint-disable-next-line no-console
    console.debug('Error occurred while injecting CSS', e);
  });
}, { url: [{ hostSuffix: 'crumbs.org' }] });


browser.runtime.onInstalled.addListener(async (details) => {
  switch (details.reason) {
    case 'install': {
      browser.tabs.create({
        active: true,
        url: `${CRUMBS_WEBSITE_URL}/thank-you/`,
      });
      break;
    }

    // // Uncomment below lines, when the update page needs to be shown
    // case 'update': {
    //   const version = process.env.ADDON_VERSION;
    //
    //   const totalTrackers = await stats.get('total_trackers');
    //   browser.tabs.create({
    //     active: true,
    //     url: `${CRUMBS_WEBSITE_URL}/what-is-new/?version=${version}&trackers=${totalTrackers}`,
    //   });
    //   break;
    // }

    default:
      // do nothing
      break;
  }
});

// Set uninstall URL for the feedback form
// eslint-disable-next-line max-len
browser.runtime.setUninstallURL(`${CRUMBS_WEBSITE_URL}/uninstall/?version=${browser.runtime.getManifest().version}`);

