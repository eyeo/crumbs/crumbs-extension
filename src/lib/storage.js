import browser from 'webextension-polyfill';


class BaseStorage {
  constructor(prefix) {
    this.prefix = `${prefix}:`;
    this.initPromise = null;
    this.isReady = false;
    this.data = new Map();
  }

  setDefault(keyName, value) {
    this.data.set(keyName, value);
  }

  async readFromStorage() {
    const result = await browser.storage.local.get();
    for (const key of Object.keys(result)) {
      if (key.startsWith(this.prefix)) {
        const keyName = key.replace(this.prefix, '');
        this.data.set(keyName, result[key]);
      }
    }
  }

  async writeToStorage() {
    const items = {};
    for (const [key, value] of this.data.entries()) {
      items[this.prefix + key] = value;
    }
    await browser.storage.local.set(items);
  }

  async init() {
    if (!this.initPromise) {
      this.initPromise = (async () => {
        // read values from storage
        await this.readFromStorage();
        this.isReady = true;
      })();
    }
    return this.initPromise;
  }

  async ready() {
    return this.init();
  }

  async set(keyName, value) {
    await this.ready();
    this.data.set(keyName, value);
    return browser.storage.local.set({
      [this.prefix + keyName]: value,
    });
  }

  async get(keyName) {
    await this.ready();
    return this.data.get(keyName);
  }

  async getAll() {
    await this.ready();
    return Object.fromEntries(this.data.entries());
  }

  async remove(keys) {
    await this.ready();
    const keysArray = !Array.isArray(keys) ? [keys] : keys;
    const keysToDelete = [];
    keysArray.forEach((key) => {
      keysToDelete.push(this.prefix + key);
      this.data.delete(key);
    });
    return browser.storage.local.remove(keysToDelete);
  }
}

export default BaseStorage;
