import browser from 'webextension-polyfill';

import TabManager from './tab-manager';
import port from './port';
import BaseStorage from './storage';
import Icons from './icons';
import { prettifyNumber } from './utils';


const emptyPageStats = {
  trackers: 0,
  cookies: 0,
};

class Stats extends BaseStorage {
  constructor() {
    super('stats');

    // set defaults
    this.setDefault('total_trackers', 0);
  }

  async updateTotalTrackers(count) {
    const totalTrackers = await this.get('total_trackers');
    await this.set('total_trackers', totalTrackers + count);
  }

  /**
   * Update the page statistics and notify the UI about the changes.
   *
   * @param tabId {Number} ID of the tab
   * @param changes
   * @param changes.trackers {Number} How many trackers were blocked on the page
   * @param changes.cookies {Number} How many cookies were expired on the page
   * @returns {Promise<void>}
   */
  async incrPageStats(tabId, changes = { ...emptyPageStats }) {
    const pageStats = await TabManager.getStats(tabId) || { ...emptyPageStats };

    if (changes.trackers) {
      pageStats.trackers += changes.trackers;
      await this.updateTotalTrackers(changes.trackers);
    }

    if (changes.cookies) {
      pageStats.cookies += changes.cookies;
    }

    await TabManager.setStats(tabId, pageStats);
    // notify UI that stats changed, so it can pull the updates
    port.notify('crumbs.statsChanged');

    await this.scheduleBadgeUpdate(tabId);
  }

  async incrTrackersBlocked(tabId) {
    return this.incrPageStats(tabId, { trackers: 1 });
  }

  async incrCookiesExpired(tabId, nr = 1) {
    return this.incrPageStats(tabId, { cookies: nr });
  }

  async delete(tabId, updateBadge = false) {
    await TabManager.removeStats(tabId);
    if (updateBadge) {
      // notify UI that stats changed, so it can pull the updates
      port.notify('crumbs.statsChanged');
      await this.scheduleBadgeUpdate(tabId);
    }
  }

  async scheduleBadgeUpdate(tabId) {
    const pageStats = await TabManager.getStats(tabId);
    const trackersBlocked = pageStats?.trackers || 0;

    const text = trackersBlocked ? prettifyNumber(trackersBlocked) : '';

    await Icons.setBadge(tabId, text);
  }
}

const stats = new Stats();


port.on('crumbs.getStats', async ({ tab }) => {
  const pageStats = await TabManager.getStats(tab.id) || { ...emptyPageStats };

  return {
    totalTrackers: await stats.get('total_trackers'),
    page: pageStats,
  };
});

browser.tabs.onActivated.addListener(async ({ tabId }) => {
  await stats.scheduleBadgeUpdate(tabId);
});

browser.webNavigation.onCommitted.addListener((details) => {
  const { tabId, frameId } = details;
  if (frameId === 0) {
    stats.delete(tabId, true);
  }
}, { url: [{ schemes: ['https', 'http'] }] });


// TODO (alexm): activate action count when we can filter out DNT & GPC matches
browser.declarativeNetRequest.setExtensionActionOptions({
  displayActionCountAsBadgeText: false,
});


export default stats;
