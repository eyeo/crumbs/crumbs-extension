/* eslint-disable import/prefer-default-export */

/**
 * Formats given number into human-readable
 * e.g. 24K, 1M...
 *
 * @param n {number}
 * @returns {string}
 */
export function prettifyNumber(n) {
  return new Intl.NumberFormat('en-GB', {
    notation: 'compact',
    compactDisplay: 'short',
  }).format(n);
}
