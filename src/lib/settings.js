// import CookiePolicyEnum from src as settings module is used in email-relay content script and
// tree shaking is not working
import { CookiePolicyEnum } from '@eyeo/crumbs-ext-sdk/src/cookies/constants';
import BaseStorage from './storage';


class Settings extends BaseStorage {
  constructor() {
    super('settings');
    // set defaults
    this.setDefault('tos', false);
    this.setDefault('hide_referrer', true);
    this.setDefault('dnt', true);
    this.setDefault('remove_marketing_parameters', true);
    this.setDefault('block_cmps', false);
    this.setDefault('gpc', true);
    this.setDefault('anti_fingerprinting', true);
    this.setDefault('api_key', '');
    this.setDefault('relay', true);
    this.setDefault('share_statistics', true);
    this.setDefault('cookies_policy', CookiePolicyEnum.EXPIRE);
  }
}

const settings = new Settings();
settings.init();
export default settings;
