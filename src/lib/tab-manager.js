import browser from 'webextension-polyfill';


class TabManager {
  /**
   * Return the url for the specified tab id
   *
   * @param {int} tabId
   * @returns the main document url
   */
  static async getUrl(tabId) {
    try {
      const tab = await browser.tabs.get(tabId);
      return tab.url;
    } catch {
      return null;
    }
  }

  /**
   * Checks whether the tab is incognito/private
   *
   * @param {int} tabId The ID of the tab
   * @returns {boolean}
   */
  static async isPrivate(tabId) {
    try {
      const tab = await browser.tabs.get(tabId);
      return tab.incognito;
    } catch {
      return false;
    }
  }

  static async setStats(tabId, data) {
    return browser.storage.session.set({ [`${tabId}:s`]: data });
  }

  static async getStats(tabId) {
    const key = `${tabId}:s`;
    const result = await browser.storage.session.get(key);
    return result[key];
  }

  static async removeStats(tabId) {
    return browser.storage.session.remove(`${tabId}:s`);
  }

  static async remove(tabId) {
    return Promise.all([
      TabManager.removeStats(tabId),
    ]);
  }
}

// listen for tab changes and clean up the memory
browser.tabs.onRemoved.addListener((tabId) => {
  TabManager.remove(tabId);
});
browser.tabs.onReplaced.addListener((_, removedTabId) => {
  TabManager.remove(removedTabId);
});

export default TabManager;
