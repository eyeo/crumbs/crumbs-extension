import browser from 'webextension-polyfill';
import AllowList from 'adblocker/src/allowlist';


const DISABLED_ICONS_PATH = '/icons/crumbs-allowed-$size.png';
const ENABLED_ICONS_PATH = '/icons/crumbs-$size.png';
const ACTIVE_BADGE_COLOR = '#00C49A';
const DISABLES_BADGE_COLOR = '#FF4F06';


class Icons {
  static setIcon(tabId, iconPath) {
    browser.action.setIcon(
      {
        tabId,
        path: {
          16: iconPath.replace('$size', '16'),
          20: iconPath.replace('$size', '20'),
          32: iconPath.replace('$size', '32'),
          40: iconPath.replace('$size', '40'),
          48: iconPath.replace('$size', '48'),
          64: iconPath.replace('$size', '64'),
          128: iconPath.replace('$size', '128'),
        },
      },
    ).catch(() => {
      // ignore error
    });
  }

  static setDisabledIcons(tabId) {
    browser.action.setBadgeBackgroundColor({
      tabId,
      color: DISABLES_BADGE_COLOR,
    }).catch(() => {
      // ignore error
    });
    Icons.setIcon(tabId, DISABLED_ICONS_PATH);
  }

  static setEnabledIcons(tabId) {
    browser.action.setBadgeBackgroundColor({
      tabId,
      color: ACTIVE_BADGE_COLOR,
    }).catch(() => {
      // ignore error
    });
    Icons.setIcon(tabId, ENABLED_ICONS_PATH);
  }

  static async setBadge(tabId, text) {
    try {
      await browser.action.setBadgeText({
        tabId,
        text,
      });
    } catch (err) {
      // console.error("Can't update badge text", err);
    }
  }
}

export default Icons;

browser.action.setBadgeBackgroundColor({ color: ACTIVE_BADGE_COLOR }).catch(() => {
  // ignore error
});

// Change text color to white on Firefox.
if (browser.action.setBadgeTextColor) {
  browser.action.setBadgeTextColor({ color: '#FFFFFF' }).catch(() => {
    // ignore error
  });
}


browser.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  if (changeInfo.status === 'complete') {
    if (await AllowList.has(tab.url, tabId)) {
      Icons.setDisabledIcons(tabId);
    }
  }
});
