import BaseStorage from '../storage';
import port from '../port';
import relayAPI from './relay-api';


class RelayStorage extends BaseStorage {
  constructor() {
    super('relay-associations');
  }

  async getAssociatedAlias(domain) {
    const aliases = await this.get(domain);
    // Return only last associated alias, this will change in the future
    // when we will have a UI to display all associated aliases
    if (aliases && Array.isArray(aliases) && aliases.length > 0) {
      return aliases[aliases.length - 1];
    }

    return null;
  }

  async setAssociatedAlias(domain, alias) {
    const previousAliases = await this.get(domain);
    let associatedAliases = [alias];
    if (previousAliases) {
      associatedAliases = previousAliases;
      if (associatedAliases.indexOf(alias) === -1) {
        associatedAliases.push(alias);
      }
    }
    return this.set(domain, associatedAliases);
  }

  async getAliasToDomainMapping() {
    await this.ready();

    const result = {};
    for (const [domain, aliases] of this.data.entries()) {
      for (const alias of aliases) {
        if (!(alias in result)) {
          result[alias] = [];
        }
        result[alias].push(domain);
      }
    }
    return result;
  }
}

const relayStorage = new RelayStorage();
relayStorage.init();
export default relayStorage;

port.on('relay.getAliasToDomainMap', async () => {
  const mapping = await relayStorage.getAliasToDomainMapping();
  return mapping;
});

port.on('relay.get', async (message, sender) => {
  const { host } = new URL(sender.tab.url);
  return relayStorage.getAssociatedAlias(host);
});

port.on('relay.generateAlias', async (message, sender) => {
  const { host } = new URL(sender.tab.url);

  const alias = await relayAPI.createAlias();

  // If the alias generation fails, then we should not set an association.
  if (alias) {
    await relayStorage.setAssociatedAlias(host, alias);
  }

  return alias;
});

port.on('relay.sync', async () => {
  const aliases = await relayAPI.getAliases();

  // If the fetch of aliases fails, then we should not sync the locally stored aliases.
  if (!aliases) {
    return false;
  }

  const domainsToDelete = [];
  await relayStorage.ready();
  for (const [domain, associatedAliases] of relayStorage.data.entries()) {
    const syncedAliases = associatedAliases.filter((alias) => aliases.has(alias));
    if (syncedAliases.length !== associatedAliases.length) {
      if (syncedAliases.length === 0) {
        domainsToDelete.push(domain);
      } else {
        relayStorage.data.set(domain, syncedAliases);
      }
    }
  }

  try {
    await relayStorage.remove(domainsToDelete);
    await relayStorage.writeToStorage();
  } catch (e) {
    console.error(e);
    return false;
  }

  return true;
});
