import settings from '../settings';
import { CRUMBS_RELAY_API_URL } from '../../../config';


class RelayApi {
  async createAlias() {
    const apiKey = await settings.get('api_key');

    try {
      const response = await fetch(`${CRUMBS_RELAY_API_URL}/v1.0/aliases/`, {
        method: 'POST',
        headers: { Authorization: `Bearer ${apiKey}` },
      });

      if (!response.ok) {
        throw new Error(`HTTP Error ${response.status}`);
      }

      const alias = await response.json();

      return alias.display_address;
    } catch (e) {
      console.error('Failed to generate a new email alias.', e);
    }

    return '';
  }

  async getAliases() {
    const apiKey = await settings.get('api_key');

    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${apiKey}`,
      },
    };

    try {
      const response = await fetch(`${CRUMBS_RELAY_API_URL}/v1.0/aliases`, options);
      if (!response.ok) {
        return false;
      }

      const aliases = await response.json().catch(() => ({}));

      return new Set(aliases.map((alias) => alias.display_address));
    } catch (e) {
      console.error('Failed to fetch the aliases.', e);
    }

    return false;
  }
}

const relayAPI = new RelayApi();
export default relayAPI;
