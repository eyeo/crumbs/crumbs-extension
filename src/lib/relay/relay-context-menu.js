import browser from 'webextension-polyfill';

import relayStorage from './relay-storage';
import settings from '../settings';
import port from '../port';
import TabManager from '../tab-manager';
import { CRUMBS_RELAY_URL } from '../../../config';
import relayAPI from './relay-api';


const openRelayDashboard = () => {
  browser.tabs.create({
    active: true,
    url: `${CRUMBS_RELAY_URL}/account/profile`,
  });
};

const sendData = (tabId, alias, type) => {
  browser.tabs.sendMessage(tabId, {
    type,
    value: alias,
  });
};

const fillInput = (tabId, alias) => {
  sendData(tabId, alias, 'contextmenu.fill_input');
};

const copyToClipboard = (tabId, alias) => {
  sendData(tabId, alias, 'contextmenu.copy');
};

const updateAssociatedAlias = (alias) => {
  const extra = alias ? { title: alias } : {};
  browser.contextMenus.update('alias', { visible: !!alias, ...extra }).catch(() => { /* ignore errors */ });
  browser.contextMenus.update('separator', { visible: !!alias }).catch(() => { /* ignore errors */ });
};

const generateAliases = async (tab) => {
  const apiKey = await settings.get('api_key');

  if (!apiKey) {
    openRelayDashboard();
    return;
  }

  const domain = new URL(tab.url).host;
  try {
    const alias = await relayAPI.createAlias();
    if (alias) {
      await relayStorage.setAssociatedAlias(domain, alias);
      updateAssociatedAlias(alias);
      fillInput(tab.id, alias);
      copyToClipboard(tab.id, alias);
    }
  } catch (e) {
    console.error('Failed to generate a new alias from context-menu', e);
  }
};

const contextMenu = [
  {
    id: 'crumbsEmailRelay',
    title: browser.i18n.getMessage('options_relay_title'),
    contexts: ['all'],
    documentUrlPatterns: ['http://*/*', 'https://*/*'],
  },
  {
    id: 'alias',
    parentId: 'crumbsEmailRelay',
    title: 'alias',
    contexts: ['all'],
    visible: false,
  },
  {
    id: 'fill',
    parentId: 'alias',
    title: 'Fill',
    contexts: ['editable'],
  },
  {
    id: 'copy',
    parentId: 'alias',
    title: 'Copy',
    contexts: ['all'],
  },
  {
    id: 'separator',
    parentId: 'crumbsEmailRelay',
    type: 'separator',
    contexts: ['all'],
    visible: false,
  },
  {
    id: 'generateAliases',
    parentId: 'crumbsEmailRelay',
    title: browser.i18n.getMessage('options_relay_generate'),
    contexts: ['all'],
  },
  {
    id: 'manageAliases',
    parentId: 'crumbsEmailRelay',
    title: browser.i18n.getMessage('options_relay_manage'),
    contexts: ['all'],
  },
];

/**
 *
 * @param url {string|null} The URL address or null
 */
const updateAssociationForURL = (url) => {
  const host = url ? new URL(url).host : '';
  relayStorage.getAssociatedAlias(host).then((alias) => {
    updateAssociatedAlias(alias);
  });
};

port.on('contextmenu.update_alias', (message, sender) => {
  updateAssociationForURL(sender.tab.url);
});

browser.tabs.onActivated.addListener(async ({ tabId }) => {
  const pageURL = await TabManager.getUrl(tabId);
  updateAssociationForURL(pageURL);
});

browser.webNavigation.onCommitted.addListener(
  (details) => {
    const { url, frameId } = details;

    if (frameId === 0) {
      updateAssociationForURL(url);
    }
  },
  {
    url: [
      { schemes: ['http', 'https'] },
    ],
  },
);

const removeContextMenu = () => {
  browser.contextMenus.removeAll();
};

const addContextMenu = () => {
  // First remove any potentially existing context menu before trying to create it.
  removeContextMenu();
  for (const menuEntry of contextMenu) {
    browser.contextMenus.create(menuEntry);
  }

  browser.contextMenus.onClicked.addListener(async (info, tab) => {
    switch (info.menuItemId) {
      case 'generateAliases':
        await generateAliases(tab);
        break;

      case 'manageAliases':
        openRelayDashboard();
        break;

      case 'fill': {
        const domain = new URL(tab.url).host;
        const alias = await relayStorage.getAssociatedAlias(domain);
        fillInput(tab.id, alias);
        break;
      }

      case 'copy': {
        const domain = new URL(tab.url).host;
        const alias = await relayStorage.getAssociatedAlias(domain);
        copyToClipboard(tab.id, alias);
        break;
      }

      default:
        console.warn('Unknown handler for contextMenu item:', info.menuItemId);
        break;
    }
  });
};

const toggleMenu = (isEnabled) => {
  if (isEnabled) {
    addContextMenu();
  } else {
    removeContextMenu();
  }
};

settings.get('relay').then((relayEnabled) => {
  toggleMenu(relayEnabled);
});

export default toggleMenu;
