/**
 * Wrapper to be used for listening on browser.runtime.onMessage messages.
 *
 * Returning a Promise from the listener seems to be the new preferred way of sending responses
 * as stated in the MDN docs:
 * https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
 */
import browser from 'webextension-polyfill';


class Port {
  constructor() {
    this.eventMap = new Map();
    this.uiPorts = new Set();
  }

  dispatch(message, sender) {
    const listener = this.eventMap.get(message.type);
    let response = new Promise((resolve) => { resolve(undefined); });
    if (listener) {
      response = listener(message, sender);
    }

    return response;
  }

  /**
   * Notify open long-lived ports about the changes
   * @param eventName
   */
  notify(eventName) {
    for (const uiPort of this.uiPorts) {
      try {
        uiPort.postMessage({
          type: eventName,
          changed: true,
        });
      } catch (e) {
        this.uiPorts.delete(uiPort);
      }
    }
  }

  on(name, listener) {
    this.eventMap.set(name, (...args) => {
      const response = listener(...args);
      if (response && typeof response.then === 'function') {
        return response;
      }

      return new Promise((resolve) => { resolve(response); });
    });
  }
}

const port = new Port();

browser.runtime.onMessage.addListener((message, sender) => port.dispatch(message, sender));
browser.runtime.onConnect.addListener((uiPort) => {
  if (uiPort.name !== 'ui') {
    return;
  }

  port.uiPorts.add(uiPort);

  uiPort.onDisconnect.addListener(() => {
    port.uiPorts.delete(uiPort);
  });
});


export default port;
