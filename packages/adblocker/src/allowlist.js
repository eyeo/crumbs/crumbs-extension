import browser from 'webextension-polyfill';
import * as EWE from '@eyeo/webext-ad-filtering-solution';


const extensionProtocol = new URL(browser.runtime.getURL('')).protocol;

class AllowList {
  /**
   * Add an allow filter for a certain host.
   *
   * @param url The url to be allowed.
   */
  static async add(url) {
    const hostname = new URL(url).hostname.replace(/^www\./, '');
    await EWE.filters.add([`@@||${hostname}^$document`]);
  }

  /**
   * Removes the allow filter for a certain host.
   *
   * @param url The url to be removed.
   */
  static async remove(url) {
    const hostname = new URL(url).hostname.replace(/^www\./, '');
    await EWE.filters.remove([`@@||${hostname}^$document`]);
  }


  /**
   * Checks if a specific hostname is allowed.
   *
   * @param url The url to be checked.
   * @param tabId The id of the tab where the request to the url was made
   * @returns {Promise<boolean>} True if the hostname is allowed, False otherwise.
   */
  static async has(url, tabId) {
    try {
      const parsedUrl = new URL(url);

      // Extension or browser pages are always whitelisted
      if (
        parsedUrl.protocol === extensionProtocol
        || parsedUrl.protocol === 'chrome:'
        || parsedUrl.protocol === 'about:'
      ) {
        return true;
      }
    } catch (e) {
      // console.error('Unable to parse invalid URL', url);
      return true;
    }

    return EWE.filters.isResourceAllowlisted(url, 'document', tabId, 0);
  }
}

export default AllowList;
