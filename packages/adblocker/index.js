import * as EWE from '@eyeo/webext-ad-filtering-solution';
import subscriptions from './custom-subscriptions.json';


class AdBlock {
  static async init(addonInfo) {
    await EWE.start(addonInfo);
    await AdBlock.initializeSubscriptions();
  }

  static async initializeSubscriptions() {
    const promises = [];
    for (const subscription of subscriptions) {
      promises.push(EWE.subscriptions.add(subscription.url));
    }
    return Promise.all(promises);
  }

  /**
   * Disables/Enables subscription if already exists in filterStorage
   *
   * @param state
   * @returns {Promise<boolean>}
   */
  static async toggleCMPSubscription(state = true) {
    const subscription = subscriptions.find(({ type }) => type === 'cookies');
    if (!state) {
      await EWE.subscriptions.disable(subscription.url);
    } else {
      await EWE.subscriptions.enable(subscription.url);
    }

    return true;
  }

  static onBlocked(callback) {
    EWE.reporting.onBlockableItem.addListener(callback, {
      filterType: 'blocking',
    });
  }
}

export default AdBlock;
