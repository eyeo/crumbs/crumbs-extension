<p align="center">
    <img src="app/assets/icons/full-logo.svg" width="200" />
</p>

---
We build the engine that powers media and empowers the online user.
The internet is rapidly transforming. Crumbs is the new standard for online targeting that respects privacy and user choice.

This repository is an implementation based on the [SPECTACLE](https://gitlab.com/eyeo/lab/spectacle) proposal.

## Installation
#### Install NVM (optional)
We use NVM to select and install the appropriate NodeJs version.
**https://github.com/nvm-sh/nvm/blob/master/README.md#installing-and-updating**

To install the required node version just run:
```shell script
$ nvm install
```
To use the required node version just run:
```shell script
$ nvm use
```

### Configure access to gitlab package repository `@eyeo/telemetry-*`
Create a personal access token following the steps from [GitLab Access Tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)
and select the following scopes:
- api
- read_repository

### Configure .npmrc

In order to install packages from gitlab package repository, a `.npmrc` file needs to be created in the project root. The file should have the
following format:
```
@eyeo:registry=https://gitlab.com/api/v4/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken="YOUR_AUTH_TOKEN"
//gitlab.com/api/v4/projects/27103759/packages/npm/:_authToken="YOUR_AUTH_TOKEN"
```

#### Install local npm packages
```shell script
$ npm install
```
A post-install script will autogenerate the secrets file, make sure to update them accordingly.

## Building
The following commands are available for building:
```shell script
# Build all sources
$ npm run build.dev
```

```shell script
# Build for production
$ npm run build.prod
```

```shell script
# Build and watch for changes
$ npm run build.watch
```
After building the extension you can load it in the browser from `/dist` folder.

## Run tests
> ❗❗❗ The tests are running on a Chromium browser only using playwright.

```shell script
npm run build.prod
npm run test # run all tests
npm run test tests/headers-removed.spec.js # run specific test
```

## Update MV3 rules
The following command will use `packages/adblockers/custom-subscriptions.json` and pass that to `@eyeo/webext-ad-filtering-solution`
scripts in order to fetch and convert the lists from MV2 to MV3 format. Resulting lists will be found in the `rulesets` directory.
```bash
$ npm run update.rules
```


## Create deploy artifacts
To create the artifacts run manually the `release` job from Gitlab CI.
This will add a new [release and a tag](https://gitlab.com/eyeo/crumbs/crumbs-extension/-/releases)
with the `.zip, .xpi, .crx` files attached. It also updates the MV3 rulesets.


## Acknowledgments

Special thanks to [BrowserStack](https://www.browserstack.com/) for supporting this project.


## License

Licensed under GPL 3.0;
you may not use this files except in compliance with the License.
You may obtain a copy of the License at https://www.gnu.org/licenses/gpl-3.0.en.html
